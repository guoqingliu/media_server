//
// Created by rio on 3/5/18.
//

#include "h264_session_map.h"

h264_session_map::h264_session_map(const char *repos) {
    return;
    _buf_max_size = H264_SESSION_BUF_SIZE;
    if ((_buf = av_malloc(_buf_max_size)) == 0)
        throw;
    _buf_size = 0;

    char repos_copy[0x200];
    char *host, *port, *pos;
    uint16_t num_port;

    strcpy(repos_copy, repos);
    host = strstr(repos_copy, "hdfs://");
    host += 7;
    port = strstr(host, ":");
    *port = 0;
    ++port;
    pos = strstr(port, "/");
    *pos = 0;
    num_port = atoi(port);
    strcpy(_repos, repos);
    if ((_fs = hdfsConnect(host, num_port)) == 0)
        throw;
}

h264_session_map::~h264_session_map() {

    session_map_t::iterator it;

    for (it = _session_map.begin(); it != _session_map.end();) {
        delete it->second;
        it = _session_map.erase(it);
    }
    av_freep(&_buf);
    hdfsDisconnect(_fs);
}

int h264_session_map::setup(size_t *id, char lab, char dev, char chn, size_t begin_pts, size_t end_pts) {
    h264_session *session = new h264_session(_fs, _repos);
    if (session->setup(id, lab, dev, chn, begin_pts, end_pts) < 0) {
        //log("setup failed");
        return -1;
    }
    _session_map[*id] = session;
    //log("setup ok");
    return 0;
}

int h264_session_map::play(size_t id, void *data, size_t data_size, size_t *pts) {
    session_map_t::iterator it;

    if ((it = _session_map.find(id)) == _session_map.end()) {
        log("play failed", "find session by id=%zu failed", id);
        return -1;
    }
    return it->second->play(data, data_size, pts);
}

int h264_session_map::seek(size_t id, size_t pts) {
    session_map_t::iterator it;

    if ((it = _session_map.find(id)) == _session_map.end()) {
        log("seek failed", "find session by id=%zu failed", id);
        return -1;
    }
    return it->second->seek(pts);
}

int h264_session_map::teardown(size_t id) {
    session_map_t::iterator it;

    if ((it = _session_map.find(id)) == _session_map.end()) {
        log("teardown failed", "find session by id=%zu failed", id);
        return -1;
    }
    delete it->second;
    _session_map.erase(it);
    return 0;
}

int h264_session_map::play(size_t id, void **data, size_t *data_size, size_t *pts) {
    int ret;
    if ((ret = play(id, _buf, _buf_max_size, pts)) < 0) {
        return -1;
    }
    _buf_size = ret;
    *data = _buf;
    *data_size = _buf_size;
    return _buf_size;
}
