//
// Created by rio on 2/7/18.
//

#include <hdfs.h>
#include "h264_source.h"

void h264_source::build() {
    //log("build", "repos=%s", _repos.c_str());
    loop_hdfs_file_node(_repos,
                   [](hdfsFileInfo* id_key_pos){
                       if(id_key_pos->mKind == kObjectKindDirectory &&
                          strcmp(id_key_pos->mName, ".") &&
                          strcmp(id_key_pos->mName, ".."))
                       {
                           return true;
                       }
                       return false;
                   },
                   [&](const std::string& id_key_path){
                       //log("build", "key=%s", id_key_path.c_str());
                       loop_hdfs_file_node(id_key_path.c_str(),
                                      [](hdfsFileInfo* year_pos){
                                          if(year_pos->mKind == kObjectKindDirectory &&
                                             strcmp(year_pos->mName, ".") &&
                                             strcmp(year_pos->mName, ".."))
                                              return true;
                                          return false;
                                      },
                                      [&](const std::string& year_path){
                                          //log("build", "year=%s", year_path.c_str());
                                          loop_hdfs_file_node(year_path.c_str(),
                                                         [](hdfsFileInfo* month_pos){
                                                             if(month_pos->mKind == kObjectKindDirectory &&
                                                                strcmp(month_pos->mName, ".") &&
                                                                strcmp(month_pos->mName, ".."))
                                                                 return true;
                                                             return false;
                                                         },
                                                         [&](const std::string& month_path){
                                                             //log("build", "month=%s", month_path.c_str());
                                                             loop_hdfs_file_node(month_path.c_str(),
                                                                            [](hdfsFileInfo* day_pos){
                                                                                if(day_pos->mKind == kObjectKindDirectory &&
                                                                                   strcmp(day_pos->mName, ".") &&
                                                                                   strcmp(day_pos->mName, ".."))
                                                                                    return true;
                                                                                return false;
                                                                            },
                                                                            [&](const std::string& day_path){
                                                                                //log("build", "day=%s", day_path.c_str());
                                                                                loop_hdfs_file_node(day_path.c_str(),
                                                                                               [](hdfsFileInfo* video_pos){
                                                                                                   if(video_pos->mKind == kObjectKindFile)
                                                                                                       return true;
                                                                                                   return false;
                                                                                               },
                                                                                               [&](const std::string& video_path){
                                                                                                   //log("build", "video=%s", video_path.c_str());
                                                                                                   const char* rel_path = &*video_path.begin() + _repos.size();
                                                                                                   h264_source_key key;
                                                                                                   h264_source_value value;
                                                                                                   //log("build", "rel_path=%s", rel_path);
                                                                                                   if(sscanf(rel_path,
                                                                                                             "/%03d%03d%03d/%04d/%02d/%02d/%zu_%zu.mp4",
                                                                                                             &key._lab, &key._dev, &key._chn,
                                                                                                             &key._year, &key._month, &key._day,
                                                                                                             &value._begin_pts, &value._end_pts
                                                                                                   ) == 8)
                                                                                                   {
                                                                                                       if(_filter.check(key) && _filter.check(value))
                                                                                                       {
                                                                                                           value._full_path = video_path;
                                                                                                           source_map_t::iterator it;
                                                                                                           if((it = _source_map.find(key)) == _source_map.end())
                                                                                                           {
                                                                                                               std::vector<h264_source_value> videos;
                                                                                                               _source_map[key] = videos;
                                                                                                           }
                                                                                                           _source_map[key].push_back(value);
                                                                                                       }
                                                                                                   }

                                                                                               });
                                                                            });
                                                         });
                                      });
                   });
    _next_key = _source_map.begin();
    if(_next_key != _source_map.end())
        _next_value = _next_key->second.begin();
}

void h264_source::dump() {
    source_map_t::iterator it;

    for(it = _source_map.begin(); it != _source_map.end(); ++it)
    {
        it->first.dump();
        printf("\n");

        for(auto& m : it->second)
        {
            m.dump();
            printf("\n");
        }
    }
}

h264_source::h264_source(hdfsFS fs, const char *repos) {
    _repos = repos;
    _fmt_ctx = 0;
    _fs = fs;
    av_init_packet(&_pkt);
    if(h264_source::_g_bsfc == 0)
        h264_source::_g_bsfc = av_bitstream_filter_init("h264_mp4toannexb");
}

void h264_source_value::dump() const {
    printf("path=%s", _full_path.c_str());
}

bool operator<(const h264_source_key &l, const h264_source_key &r) {
    size_t l_value = 0, r_value = 0;

    l_value = l._year;
    l_value << 8;
    l_value += l._month;
    l_value << 8;
    l_value += l._day;
    l_value << 16;
    l_value += l._lab;
    l_value << 8;
    l_value += l._dev;
    l_value << 8;
    l_value += l._chn;

    r_value = r._year;
    r_value << 8;
    r_value += r._month;
    r_value << 8;
    r_value += r._day;
    r_value << 16;
    r_value += r._lab;
    r_value << 8;
    r_value += r._dev;
    r_value << 8;
    r_value += r._chn;

    return l_value < r_value;
}

void h264_source_key::dump() const {
    printf("lab=%03d, dev=%03d, chn=%03d, year=%04d, month=%02d, day=%02d",
           _lab, _dev, _chn, _year, _month, _day
    );
}


AVBitStreamFilterContext* h264_source::_g_bsfc = 0;