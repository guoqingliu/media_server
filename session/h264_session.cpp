//
// Created by rio on 2/7/18.
//

#include "h264_session.h"

id_gen h264_session::__id_gen;

h264_session::h264_session(hdfsFS fs, const char *repos) :
        _source(fs, repos)
{
    log("h264_session", "fs=%p, repos=%s", fs, repos);
}

int h264_session::setup(size_t *id, char lab, char dev, char chn, size_t begin_pts, size_t end_pts) {
    //gen id
    _lab = lab;
    _dev = dev;
    _chn = chn;
    _begin_pts = begin_pts;
    _end_pts = end_pts;

    if(setup_impl() == 0)
    {
        *id = _id;
        log("setup ok", "lab=%d, dev=%d, chn=%d, begin_pts=%zu, end_pts=%zu, id=%zu",
            lab, dev, chn, begin_pts, end_pts, _id);
        return 0;
    }
    log("setup failed", "lab=%d, dev=%d, chn=%d, begin_pts=%zu, end_pts=%zu",
        lab, dev, chn, begin_pts, end_pts);
    return -1;
}

int h264_session::play(void *data, size_t data_size, size_t *pts) {
    int ret = _source.read_next_frame(data, data_size, pts);
    //log("play", "data=%p, data_size=%zu, pts=%zu, ret=%d", data, data_size, *pts, ret);
    return ret;
}

int h264_session::seek(size_t pts) {
    int ret = _source.seek_frame(pts);
    log("seek", "pts=%zu, ret=%d", pts, ret);
    return ret;
}

int h264_session::setup_impl() {
    _source.set_filter(_lab, _dev, _chn, _begin_pts, _end_pts);
    _source.build();
    //_source.dump();
    if(_source.seek_frame(_begin_pts) < 0)
        return -1;
    _id = __id_gen.get();
    return 0;
}
