//
// Created by rio on 2/7/18.
//

#ifndef MEDIA_SERVER_H264_SOURCE_H
#define MEDIA_SERVER_H264_SOURCE_H

#include <cstring>
#include <ctime>
#include <cstdint>
#include <cstdio>
#include <vector>
#include <map>
#include <dirent.h>
#include <hdfs.h>
#include "../util/util.h"

class h264_source_key
{
public:
    uint8_t _lab;
    uint8_t _dev;
    uint8_t _chn;
    uint16_t _year;
    uint8_t _month;
    uint8_t _day;

    friend bool operator<(const h264_source_key& l, const h264_source_key& r);

    void dump() const;

    void get_pts_range(size_t* begin_pts, size_t* end_pts)
    {
        tm tm;
        time_t begin_time, end_time;

        tm.tm_year = _year - 1900;
        tm.tm_mon = _month - 1;
        tm.tm_mday = _day;
        tm.tm_hour = 0;
        tm.tm_min = 0;
        tm.tm_sec = 0;

        begin_time = mktime(&tm);
        end_time = begin_time + 3600 * 24;

        *begin_pts = gen_pts(&begin_time);
        *end_pts = gen_pts(&end_time);
    }
};

class h264_source_value
{
public:
    size_t _begin_pts;
    size_t _end_pts;
    std::string _full_path;

    void dump() const;
};

class h264_source_filter
{
public:
    uint8_t _lab;
    uint8_t _dev;
    uint8_t _chn;
    size_t _begin_pts;
    size_t _end_pts;

    bool check(h264_source_key key)
    {
        size_t begin_pts, end_pts;

        if(key._lab != _lab || key._dev != _dev || key._chn != _chn)
            return false;

        key.get_pts_range(&begin_pts, &end_pts);

        if(_end_pts < begin_pts || _begin_pts > end_pts)
            return false;
        return true;
    }

    bool check(h264_source_value value)
    {
        if(_end_pts < value._begin_pts || _begin_pts > value._end_pts)
            return false;
        return true;
    }
};

class h264_source
{
    typedef h264_source_key source_map_key_t;
    typedef std::vector<h264_source_value> source_map_value_t;
    typedef std::map<source_map_key_t, source_map_value_t> source_map_t;
public:
    h264_source(hdfsFS fs, const char* repos);
    ~h264_source()
    {
        if(_fmt_ctx)
            avformat_close_input(&_fmt_ctx);
    }
    void build();

    template<typename _Check, typename _Callback>
    void loop_file_node(const std::string& path, _Check check, _Callback callback)
    {
        DIR* dir;
        dirent* pos;
        std::string sub_path;

        dir = opendir(path.c_str());
        if(dir == 0)
            return;

        while(pos = readdir(dir))
        {
            if(check(pos))
            {
                sub_path = path + "/";
                sub_path += pos->d_name;
                callback(sub_path);
            }
        }

        closedir(dir);
    };


    template<typename _Check, typename _Callback>
    void loop_hdfs_file_node(const std::string& path, _Check check, _Callback callback)
    {
        hdfsFileInfo* file_info,* pos;
        int file_info_num;
        std::string sub_path;

        if((file_info = hdfsListDirectory(_fs, path.c_str(), &file_info_num)) == 0)
            return;
        for(int i = 0; i < file_info_num; ++i)
        {
            pos = file_info + i;
            if(check(pos))
            {
                sub_path = pos->mName;
                callback(sub_path);
            }
        }
        hdfsFreeFileInfo(file_info, file_info_num);
    };
    void set_filter(uint8_t lab, uint8_t dev, uint8_t chn, size_t begin_pts = 0, size_t end_pts = -1)
    {
        _filter._lab = lab,
        _filter._dev = dev;
        _filter._chn = chn;
        _filter._begin_pts = begin_pts;
        _filter._end_pts = end_pts;
    }

    int check(size_t pts)
    {
        int ret;

        if(pts < _filter._begin_pts || pts > _filter._end_pts){
            log("check", "pts=%zu not in range", pts);
            return -1;
        }

        for(_next_key = _source_map.begin(); _next_key != _source_map.end(); ++_next_key)
        {
            for(_next_value = _next_key->second.begin(); _next_value != _next_key->second.end(); ++_next_value)
            {
                if(pts < _next_value->_begin_pts)
                    pts = _next_value->_begin_pts;

                if(pts >= _next_value->_begin_pts && pts <= _next_value->_end_pts)
                {
                    if(_fmt_ctx)
                        avformat_close_input(&_fmt_ctx);

                    if ((ret = avformat_open_input(&_fmt_ctx, _next_value->_full_path.c_str(), 0, 0)) < 0) {
                        log("check", "pts=%zu avformat_open_input failed:%s", pts, err_info(ret));
                        return -1;
                    }
                    if ((ret = avformat_find_stream_info(_fmt_ctx, 0)) < 0) {
                        avformat_close_input(&_fmt_ctx);
                        log("check", "pts=%zu avformat_find_stream_info failed:%s", pts, err_info(ret));
                        return -1;
                    }
                    if((ret = av_seek_frame(_fmt_ctx, -1, pts , AVSEEK_FLAG_ANY)) < 0)
                    {
                        log("check", "pts=%zu av_seek_frame failed:%s", pts, err_info(ret));
                        return -1;
                    }
                    init_sps_pps();
                    return 0;
                }
            }
        }
        log("check", "pts=%zu not in range rel", pts);
        return -1;
    }

    int check()
    {
        int ret;

        if(_fmt_ctx == 0)
        {
            if(_next_key == _source_map.end() || _next_value == _next_key->second.end())
            {
                log("check failed", "to end");
                return -1;
            }

            if ((ret = avformat_open_input(&_fmt_ctx, _next_value->_full_path.c_str(), 0, 0)) < 0) {
                log("check failed", "avformat_open_input failed:%s", err_info(ret));
                return -1;
            }
            if ((ret = avformat_find_stream_info(_fmt_ctx, 0)) < 0) {
                avformat_close_input(&_fmt_ctx);
                log("check failed", "avformat_find_stream_info failed:%s", err_info(ret));
                return -1;
            }
            init_sps_pps();
            next_index();
            return 0;
        }
        return 0;
    }

    void init_sps_pps()
    {
        static uint8_t _g_spe[] = {0, 0, 0, 1};
        uint8_t* extra_data = _fmt_ctx->streams[0]->codec->extradata;
        size_t extra_data_size = _fmt_ctx->streams[0]->codec->extradata_size;
        uint8_t* pos;

        pos = extra_data;
        pos += 0x04;

        for(;pos < extra_data + extra_data_size; ++pos)
        {
            if(*pos == 0x67)
                break;
        }
        _sps.resize(sizeof(_g_spe) + *(pos - 1));
        memcpy(const_cast<uint8_t*>(&*_sps.begin()), _g_spe, sizeof(_g_spe));
        memcpy(const_cast<uint8_t*>(&*_sps.begin()) + sizeof(_g_spe),
               pos, *(pos - 1));

        pos += *(pos - 1);
        for(; pos < extra_data + extra_data_size; ++pos)
        {
            if(*pos == 0x68)
                break;
        }
        _pps.resize(sizeof(_g_spe) + *(pos - 1));
        memcpy(const_cast<uint8_t*>(&*_pps.begin()), _g_spe, sizeof(_g_spe));
        memcpy(const_cast<uint8_t*>(&*_pps.begin()) + sizeof(_g_spe),
               pos, *(pos - 1));

        printf("extra data\n");
        print_buf(extra_data, extra_data_size);
        printf("sps\n");
        print_buf(&*_sps.begin(), _sps.size());
        printf("pps\n");
        print_buf(&*_pps.begin(), _pps.size());
    }

    int append_sps_pps(void* data, size_t data_size)
    {
        if(_sps.size() == 0 || _pps.size() == 0 ||
                data_size < _sps.size() + _pps.size())
            return -1;
        memcpy((uint8_t*)data, &*_sps.begin(), _sps.size());
        memcpy((uint8_t*)data + _sps.size(), &*_pps.begin(), _pps.size());
        return _sps.size() + _pps.size();
    }

    int next_index()
    {
        ++_next_value;
        if(_next_value == _next_key->second.end())
        {
            ++_next_key;
            if(_next_key != _source_map.end())
                _next_value = _next_key->second.begin();
        }
    }

    int read_next_frame(void* data, size_t data_size, size_t* pts)
    {
        static uint8_t _g_spe[] = {0, 0, 0, 1};
        uint8_t* h264_data;
        int h264_data_size;
        int frame_size;
        int ret;

        if(_g_bsfc == 0)
            return -1;

        if(check() < 0)
        {
            //log("read_next_frame", "check failed");
            return -1; //break loop
        }

        if((frame_size = append_sps_pps(data, data_size)) < 0)
        {
            log("read_next_frame", "append_sps_pps failed");
            return -1;
        }

        memcpy((uint8_t*)data + frame_size, _g_spe, sizeof(_g_spe));
        frame_size += sizeof(_g_spe);

        if((ret = av_read_frame(_fmt_ctx, &_pkt)) < 0)
        {
            avformat_close_input(&_fmt_ctx);
            log("read_next_frame", "av_read_frame failed:%s", err_info(ret));
            return 0; //end try next
        }

        *pts = av_rescale_q_rnd(_pkt.pts, _fmt_ctx->streams[0]->time_base, AVRational{1, TIME_US_BASE},
                                (enum AVRounding)(AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX));
        if(data_size < frame_size + _pkt.size || *pts > _filter._end_pts)
        {
            av_packet_unref(&_pkt);
            log("read_next_frame", "not enough mem or pts error");
            return -1;
        }
        if((ret = av_bitstream_filter_filter(_g_bsfc, _fmt_ctx->streams[0]->codec, 0,
                                   &h264_data, &h264_data_size, _pkt.data, _pkt.size, 0)) < 0)
        {
            av_packet_unref(&_pkt);
            log("read_next_frame", "av_bitstream_filter_filter failed:%s", err_info(ret));
            return -1;
        }

        memcpy((uint8_t*)data + frame_size, h264_data, h264_data_size);
        frame_size += h264_data_size;
        av_packet_unref(&_pkt);
        av_free(h264_data);
        return frame_size;
    }

    int seek_frame(size_t pts)
    {
        return check(pts);
    }
    void dump();

private:
    static void log(const char *what, const char *fmt = 0, ...) {
        va_list va;
        va_start(va, fmt);
        default_vlog("h264_source", what, fmt, va);
        va_end(va);
    }
    char* err_info(int err)
    {
        av_strerror(err, _err_buf, sizeof(_err_buf));
        return _err_buf;
    }
private:
    std::vector<uint8_t> _sps;
    std::vector<uint8_t> _pps;
    AVPacket _pkt;
    AVFormatContext* _fmt_ctx;
    h264_source_filter _filter;
    source_map_t::iterator _next_key;
    source_map_value_t::iterator _next_value;
    source_map_t _source_map;
    std::string _repos;
    char _err_buf[AV_ERROR_MAX_STRING_SIZE];

    static AVBitStreamFilterContext* _g_bsfc;
    hdfsFS _fs;
};
#endif //MEDIA_SERVER_H264_SOURCE_H
