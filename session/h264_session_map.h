//
// Created by rio on 3/5/18.
//

#ifndef MEDIA_SERVER_H264_SESSION_MAP_H
#define MEDIA_SERVER_H264_SESSION_MAP_H
#include <map>
#include "h264_session.h"

#define H264_SESSION_BUF_SIZE 4 * 1024 * 1024

class h264_session_map
{
    typedef std::map<size_t, h264_session*> session_map_t;
public:
    h264_session_map(const char* repos);
    ~h264_session_map();
    int setup(size_t* id, char lab, char dev, char chn, size_t begin_pts, size_t end_pts);
    int play(size_t id, void* data, size_t data_size, size_t* pts);
    int play(size_t id, void** data, size_t* data_size, size_t* pts);
    int seek(size_t id, size_t pts);
    int teardown(size_t id);
private:
    static void log(const char *what, const char *fmt = 0, ...) {
        va_list va;
        va_start(va, fmt);
        default_vlog("h264_session_map", what, fmt, va);
        va_end(va);
    }
private:
    void* _buf;
    size_t _buf_max_size;
    size_t _buf_size;
    char _repos[0x200];
    hdfsFS _fs;
    session_map_t _session_map;
};
#endif //MEDIA_SERVER_H264_SESSION_MAP_H
