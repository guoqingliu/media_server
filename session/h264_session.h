//
// Created by rio on 2/7/18.
//

#ifndef MEDIA_SERVER_H264_SESSION_H
#define MEDIA_SERVER_H264_SESSION_H

#include <cstdlib>
#include <cstdint>
#include <mutex>
#include <vector>
#include <dirent.h>
#include "h264_source.h"

class id_gen
{
public:
    id_gen() : _id(0){}
    size_t get()
    {
        std::lock_guard<std::mutex> lock{_mtx};
        return ++_id;
    }
private:
    size_t _id;
    std::mutex _mtx;
};

class h264_session
{
public:
    h264_session(hdfsFS fs, const char* repos);
    int setup(size_t* id, char lab, char dev, char chn, size_t begin_pts, size_t end_pts);
    int play(void* data, size_t data_size, size_t* pts);
    int seek(size_t pts);
    ~h264_session()
    {
        log("teardwon", "id=%zu", _id);
    }
private:
    void log(const char *what, const char *fmt, ...) {
        va_list va;
        va_start(va, fmt);
        default_vlog("h264_session", what, fmt, va);
        va_end(va);
    }
    int setup_impl();

private:
    size_t _id; //session id
    size_t _begin_pts;
    size_t _end_pts;
    char _lab;
    char _dev;
    char _chn;
    h264_source _source;
    static id_gen __id_gen;
};
#endif //MEDIA_SERVER_H264_SESSION_H
