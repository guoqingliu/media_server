//
// Created by rio on 1/31/18.
//

#ifndef MEDIA_SERVER_H264_CACHE_H
#define MEDIA_SERVER_H264_CACHE_H

#include <cstdlib>
#include <vector>
#include <cstring>
#include "../util/util.h"

class h264_cache
{
public:
    h264_cache(size_t cache_size);
    ~h264_cache();
    int write(const void* nalu, size_t nalu_size, size_t pts);
    int read_nalu(void *nalu, size_t nalu_size, size_t *pts, size_t *pos);
    int read_sps(void* nalu, size_t nalu_size);
    int read_pps(void* nalu, size_t nalu_size);
    void log(const char* what, const char* fmt = 0, ...) const;
    void push_h264_file(const char* file);
    void dump() const
    {
        log("dump", "begin=%zu, end=%zu", _begin, _end);
    }
    void reset()
    {
        _begin = _end = 0;
        _cur_pts = AV_NOPTS_VALUE;
    }
    size_t get_size()
    {
        return _end - _begin;
    }
private:
    int make_avl(size_t size);
    void bit_write(const void* data, size_t data_size);
    void bit_read(void* data, size_t data_size, size_t* pos);
private:
    std::vector<uint8_t> _sps;
    std::vector<uint8_t> _pps;
    void* _cache;
    size_t _cache_size;
    size_t _begin;
    size_t _end;
    size_t _cur_pts;
};

#endif //MEDIA_SERVER_H264_CACHE_H
