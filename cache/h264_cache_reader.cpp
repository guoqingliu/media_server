//
// Created by rio on 1/31/18.
//

#include "h264_cache_reader.h"

h264_cache_reader::h264_cache_reader(size_t cache_size) : h264_cache(cache_size),
        _pos(0), _status(0), _pts(AV_NOPTS_VALUE)
{}


void h264_cache_reader::reset() {
    _pos = 0;
    _status = 0;
}

int h264_cache_reader::read(void *nalu, size_t nalu_size) {

    int ret;
    int pos;

    pos = 0;

    if((_status & READ_SPS) == 0)
    {
        *((uint8_t *) nalu + pos + 0) = 0;
        *((uint8_t *) nalu + pos + 1) = 0;
        *((uint8_t *) nalu + pos + 2) = 0;
        *((uint8_t *) nalu + pos + 3) = 1;
        pos += 4;
        if ((ret = read_sps((uint8_t *) nalu + pos, nalu_size - pos)) < 0)
            return -1;
        _status |= READ_SPS;
        pos += ret;
    }

    if((_status & READ_PPS) == 0) {
        *((uint8_t *) nalu + pos + 0) = 0;
        *((uint8_t *) nalu + pos + 1) = 0;
        *((uint8_t *) nalu + pos + 2) = 0;
        *((uint8_t *) nalu + pos + 3) = 1;
        pos += 4;
        if ((ret = read_pps((uint8_t *) nalu + pos, nalu_size - pos)) < 0) {
            _status = 0;
            return -1;
        }
        _status |= READ_PPS;
        pos += ret;
    }

    *((uint8_t*)nalu + pos + 0) = 0;
    *((uint8_t*)nalu + pos + 1) = 0;
    *((uint8_t*)nalu + pos + 2) = 0;
    *((uint8_t*)nalu + pos + 3) = 1;
    pos += 4;
    if((ret = read_nalu((uint8_t*)nalu + pos, nalu_size - pos, &_pts, &_pos)) < 0)
    {
        _status = 0;
        return -1;
    }
    pos += ret;
    return pos;
}

char *h264_cache_reader::err_info(int err) {
    av_strerror(err, _err_buf, sizeof(_err_buf));
    return _err_buf;
}

int h264_cache_reader::init_code_parameters_by_cache(AVCodecParameters *initialized_params) {
    AVFormatContext* fmt_ctx = 0;
    AVIOContext* avio_ctx = 0;
    uint8_t* buffer = 0,* avio_ctx_buffer = 0;
    size_t buffer_size, avio_ctx_buffer_size = 400 * 1024;
    int ret;

    if (!(fmt_ctx = avformat_alloc_context())) {
        log("init_code_parameters_by_cache", "avformat_alloc_context failed:%s", err_info(AVERROR(ENOMEM)));
        goto failed;
    }
    avio_ctx_buffer = (uint8_t*)av_malloc(avio_ctx_buffer_size);
    if (!avio_ctx_buffer) {
        log("init_code_parameters_by_cache", "av_malloc failed:%s", err_info(AVERROR(ENOMEM)));
        goto failed;
    }
    avio_ctx = avio_alloc_context(avio_ctx_buffer, avio_ctx_buffer_size,
                                  0, this, &read_packet, NULL, NULL);
    if (!avio_ctx) {
        log("init_code_parameters_by_cache", "avio_alloc_context failed:%s", err_info(AVERROR(ENOMEM)));
        goto failed;
    }
    fmt_ctx->pb = avio_ctx;
    ret = avformat_open_input(&fmt_ctx, NULL, NULL, NULL);
    if (ret < 0) {
        log("init_code_parameters_by_cache", "avformat_open_input failed:%s", err_info(AVUNERROR(ret)));
        goto failed;
    }
    ret = avformat_find_stream_info(fmt_ctx, NULL);
    if (ret < 0) {
        log("init_code_parameters_by_cache", "avformat_find_stream_info failed:%s", err_info(ret));
        goto failed;
    }
    av_dump_format(fmt_ctx, 0, 0, 0);

    if(fmt_ctx->nb_streams == 0 || fmt_ctx->streams[0]->codecpar->codec_type != AVMEDIA_TYPE_VIDEO)
    {
        log("init_code_parameters_by_cache", "streams[0] not video stream");
        goto failed;
    }

    ret = avcodec_parameters_copy(initialized_params, fmt_ctx->streams[0]->codecpar);
    if(ret < 0)
    {
        log("init_code_parameters_by_cache", "avcodec_parameters_copy failed");
        goto failed;
    }
    avformat_close_input(&fmt_ctx);
    /* note: the internal buffer could have changed, and be != avio_ctx_buffer */
    if (avio_ctx) {
        av_freep(&avio_ctx->buffer);
        av_freep(&avio_ctx);
    }
    return 0;
    failed:
    avformat_close_input(&fmt_ctx);
    /* note: the internal buffer could have changed, and be != avio_ctx_buffer */
    if (avio_ctx) {
        av_freep(&avio_ctx->buffer);
        av_freep(&avio_ctx);
    }
    reset();
    return -1;
}

int h264_cache_reader::read_packet(void *opaque, uint8_t *buf, int buf_size) {
    h264_cache_reader* reader = (h264_cache_reader*)opaque;
    return reader->read(buf, buf_size);
}
