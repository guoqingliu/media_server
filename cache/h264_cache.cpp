//
// Created by rio on 1/31/18.
//

#include "h264_cache.h"
#include "../util/util.h"
#include <nanomsg/nn.h>
#include <cstring>

h264_cache::h264_cache(size_t cache_size) :
    _cache_size(cache_size), _begin(0), _end(0), _cur_pts(AV_NOPTS_VALUE)
{
    _cache = malloc(_cache_size);
    if(!_cache)
    {
        log("malloc failed");
        throw;
    }
}

h264_cache::~h264_cache() {
    free(_cache);
}

int h264_cache::write(const void *nalu, size_t nalu_size, size_t pts)
{
    uint8_t nalu_type = *(const uint8_t*)nalu & 0x1F;

    switch(nalu_type) {
        case 0x07: //sps
            if (!_sps.size()) {
                _sps.resize(nalu_size);
                memcpy(&*_sps.begin(), nalu, nalu_size);
                return nalu_size;
            }
            return _sps.size();
        case 0x08: //pps
            if (!_pps.size()) {
                _pps.resize(nalu_size);
                memcpy(&*_pps.begin(), nalu, nalu_size);
                return nalu_size;
            }
            return _pps.size();
        default:
            {
                if(_cur_pts != AV_NOPTS_VALUE && pts < _cur_pts)
                {
                    log("write pts invalid");
                    return -1;
                }


                if (make_avl(nalu_size + sizeof(size_t) + sizeof(size_t)) < 0)
                {
                    log("make_avl failed");
                    return -1;
                } else {
                    size_t data_size;

                    data_size = nalu_size + sizeof(size_t); //pts + nalu
                    bit_write(&data_size, sizeof(size_t)); //write unit size
                    bit_write(&pts, sizeof(size_t));    //write pts
                    bit_write(nalu, nalu_size); //write nalu

                    _cur_pts = pts;
                    return nalu_size;
                }
            }
            break;
    }
}

int h264_cache::read_nalu(void *nalu, size_t nalu_size, size_t *pts, size_t *pos)
{
    if(*pos < _begin)
        *pos = _begin;
    if(*pos > _end)
        *pos = _end;

    if(_begin == _end || *pos == _end)
        return -1;


    size_t unit_size;
    size_t save_pos = *pos;
    bit_read(&unit_size, sizeof(size_t), &save_pos);
    if(nalu_size < unit_size - sizeof(size_t))
    {
        log("read_nalu mem not enough");
        return -1; //mem not enough
    }
    bit_read(pts, sizeof(size_t), &save_pos);
    unit_size -= sizeof(size_t);
    bit_read(nalu, unit_size, &save_pos);
    *pos = save_pos;
    return unit_size;
}

int h264_cache::make_avl(size_t size)
{
    size_t avl_size;
    size_t unit_size;
    size_t pos;

    if(size > _cache_size)
        return -1;

    avl_size = _cache_size - (_end - _begin);
    pos = _begin;

    while(avl_size < size)
    {
        bit_read(&unit_size, sizeof(size_t), &pos);
        pos += unit_size;
        avl_size += (sizeof(size_t) + unit_size);
    }
    _begin = pos;
    return 0;
}

void h264_cache::bit_write(const void *data, size_t data_size)
{
    for(size_t index = 0; index < data_size; ++index, ++_end)
        *((uint8_t*)_cache + (_end % _cache_size)) = *((const uint8_t*)data + index);
}

void h264_cache::bit_read(void *data, size_t data_size, size_t *pos)
{
    for(size_t index = 0; index < data_size; ++index, ++*pos)
        *((uint8_t*)data + index) = *((uint8_t*)_cache + (*pos % _cache_size));
}

int h264_cache::read_sps(void *nalu, size_t nalu_size) {
    if(!_sps.size() || _sps.size() > nalu_size)
        return -1;
    memcpy(nalu, &*_sps.begin(), _sps.size());
    return _sps.size();
}

int h264_cache::read_pps(void *nalu, size_t nalu_size) {
    if(!_pps.size() || _pps.size() > nalu_size)
        return -1;
    memcpy(nalu, &*_pps.begin(), _pps.size());
    return _pps.size();
}

void h264_cache::log(const char *what, const char *fmt, ...) const {
    va_list va;
    va_start(va, fmt);
    default_vlog("h264_cache", what, fmt, va);
    va_end(va);
}

void h264_cache::push_h264_file(const char *file) {

    uint8_t* buf;
    size_t buf_size;
    uint8_t spe[] = { 0, 0, 1 };
    uint8_t* begin,* end,* r_begin,* r_end;
    size_t pts = 0;

    int frame_index = -2;

    if(av_file_map(file, &buf, &buf_size, 0, 0) >= 0);
    {

        begin = buf + 1;
        end = buf + 1 + sizeof(spe);

        for (; end < buf + buf_size - sizeof(spe);) {
            if (memcmp(spe, end, sizeof(spe)) == 0) {
                r_begin = begin + sizeof(spe);
                r_end = *(end - 1) == 0 ? end - 1 : end;
                //print_buf(r_begin, r_end - r_begin);
                if (frame_index < 0) {
                    pts = AV_NOPTS_VALUE;
                } else {
                    pts = frame_index * TIME_BASE / FRAME_RATE;
                }
                ++frame_index;
                write(r_begin, r_end - r_begin, pts);

                begin = end;
                end += sizeof(spe);
            } else
                ++end;
        }
        if (begin != end) {
            r_begin = begin + sizeof(spe);
            r_end = buf + buf_size;
            //print_buf(r_begin, r_end - r_begin);
            if (frame_index < 0) {
                pts = AV_NOPTS_VALUE;
            } else {
                pts = frame_index * TIME_BASE / FRAME_RATE;
            }
            ++frame_index;
            write(r_begin, r_end - r_begin, pts);
        }
    }
}
