//
// Created by rio on 1/31/18.
//

#ifndef MEDIA_SERVER_H264_CACHE_READER_H
#define MEDIA_SERVER_H264_CACHE_READER_H

#include "h264_cache.h"
#define READ_SPS 1
#define READ_PPS 2
class h264_cache_reader : public h264_cache
{
public:
    h264_cache_reader(size_t cache_size);
    int read(void* nalu, size_t nalu_size);
    void reset();
    char* err_info(int err);
    int init_code_parameters_by_cache(AVCodecParameters* initialized_params);

private:
    static int read_packet(void *opaque, uint8_t *buf, int buf_size);
private:
    size_t _status;
    size_t _pos;
    size_t _counts;
    size_t _pts;
    char _err_buf[AV_ERROR_MAX_STRING_SIZE];
};

#endif //MEDIA_SERVER_H264_CACHE_READER_H
