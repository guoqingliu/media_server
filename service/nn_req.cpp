//
// Created by rio on 2/7/18.
//

#include <sys/time.h>
#include <nanomsg/reqrep.h>
#include "nn_req.h"
#include "h264_parse.h"

nn_req::nn_req(const char *url) :
        nn_helper(url, NN_REQ) {
    int send_timeout = 5000; //ms
    int recv_timeout = 5000;

    set_opt(NN_SOL_SOCKET, NN_RCVTIMEO, &recv_timeout, sizeof(int));
    set_opt(NN_SOL_SOCKET, NN_SNDTIMEO, &send_timeout, sizeof(int));
}

void nn_req::push_h264_file(const char *file_fmt) {
    h264_ffmpeg_parse* parse_arr[0x04];
    char file[0x200];
    size_t pts = 0;
    int frame_index;
    std::vector<uint8_t> buf;
    timeval cur_tv;
    timeval now_tv;
    int chn;
    int ret;


    gettimeofday(&cur_tv, 0);
    if(file_fmt == 0 || *file_fmt == 0)
        return;
    buf.resize(4000 * 1024);

    for(int chn = 0; chn < 0x04; ++chn)
    {
        snprintf(file, sizeof(file), file_fmt, chn);
        parse_arr[chn] = new h264_ffmpeg_parse(file);
    }

    while(!_break_loop)
    {

        if(frame_index < 0)
        {
            pts = AV_NOPTS_VALUE;
        }
        else
        {
            cur_tv.tv_usec += (TIME_US_BASE / FRAME_RATE);
            cur_tv.tv_sec += (cur_tv.tv_usec / TIME_US_BASE);
            cur_tv.tv_usec %= TIME_US_BASE;
            pts = cur_tv.tv_sec * TIME_MS_BASE + cur_tv.tv_usec / TIME_MS_BASE;
            gettimeofday(&now_tv, 0);
            int interval = diff_time(&cur_tv, &now_tv);
            if(interval > 0)
                usleep(interval);
        }
        ++frame_index;
        for(chn = 0; chn < 0x04; ++chn)
        {
            media_server_req req;
            video* video;
            req.set__type(cmd_type::push_video);
            video = req.mutable__push_video();
            video->clear__items();
            video_item *item = video->add__items();
            video_key *key = item->mutable__key();
            key->set__lab(0);
            key->set__dev(0);
            key->set__chn(chn);
            item->set__pts(pts);
            if((ret = parse_arr[chn]->step(const_cast<uint8_t*>(&*buf.begin()), buf.size())) < 0)
            {
                break;
            } else
            {
                item->set__nalu(&*buf.begin(), ret);
                key->set__chn(chn);
                request(req);
            }
        }
        if(chn != 0x04)
            break;
        pts += (AV_TIME_BASE / 25);
    }

    for(int chn = 0; chn < 0x04; ++chn)
    {
        delete parse_arr[chn];
    }
}


void nn_req::request(media_server_req &req) {
    int ret;

    if((ret = req.ByteSize()) > _buf_max_size)
    {
        log("SerializeToArray too many bytes");
    }
    _buf_size = ret;
    if(req.SerializeToArray(_buf, _buf_max_size) < 0)
    {
        log("SerializeToArray failed");
        return;
    }

    if(send() < 0)
    {
        return;
    }
    recv();
}

nn_pollfd nn_req::pool_fd() {
    return nn_pollfd{ .fd = _sock, .events = NN_POLLOUT };
}

int nn_req::handle(const media_server_req &req, media_server_rep &rep) {
    if((_buf_size = req.ByteSize()) > _buf_max_size)
    {
        log("no enough mem");
        return -1;
    }

    if(!req.SerializeToArray(_buf, _buf_max_size))
        return -1;

    if(send() < 0)
        return -1;
    if(recv() < 0)
        return -1;
    if(!rep.ParseFromArray(_buf, _buf_size))
        return -1;
    return 0;
}

int nn_req::push_video(char lab, char dev, char chn, void *data, size_t data_size) {
    media_server_req req;
    media_server_rep rep;
    video* items;
    video_item* item;
    video_key* item_key;

    if((items = req.mutable__push_video()) == 0)
        return -1;
    if((item = items->add__items()) == 0)
        return -1;
    if((item_key = item->mutable__key()) == 0)
        return -1;

    item_key->set__chn(lab);
    item_key->set__dev(dev);
    item_key->set__chn(chn);
    item->set__nalu(data, data_size);

    req.set__type(cmd_type::push_video);

    if(handle(req, rep) < 0)
        return -1;

    if(rep._type() == cmd_type::push_video && rep.has__push_video() && rep._push_video()._status() == 0)
        return 0;
    return -1;
}

int nn_req::get_video_setup(size_t *id, char lab, char dev, char chn, size_t begin_pts, size_t end_pts) {
    media_server_req req;
    media_server_rep rep;

    media_server_req_get_video_setup* item;
    video_key* item_key;
    if((item = req.mutable__get_video_setup()) == 0)
        return -1;
    if((item_key = item->mutable__key()) == 0)
        return -1;
    item_key->set__lab(lab);
    item_key->set__dev(dev);
    item_key->set__chn(chn);
    item->set__begin_pts(begin_pts);
    item->set__end_pts(end_pts);

    req.set__type(cmd_type::get_video);
    req.set__sub_type(cmd_sub_type::setup);

    if(handle(req, rep) < 0)
        return -1;

    if(rep._type() == cmd_type::get_video &&
       rep._sub_type() == cmd_sub_type::setup &&
       rep.has__get_video_setup() &&
       rep._get_video_setup()._status() == 0)
    {
        *id = rep._get_video_setup()._session();
        return 0;
    }
    return -1;
}

int nn_req::get_video_play(size_t id, void **data, size_t *data_size, size_t *pts) {
    media_server_req req;
    media_server_rep rep;
    media_server_req_get_video_play* item;

    if((item = req.mutable__get_video_play()) == 0)
        return -1;
    item->set__session(id);
    req.set__type(cmd_type::get_video);
    req.set__sub_type(cmd_sub_type::play);

    if(handle(req, rep) < 0)
        return -1;


    if(rep._type() == cmd_type::get_video &&
       rep._sub_type() == cmd_sub_type::play &&
       rep.has__get_video_play() &&
       rep._get_video_play()._status() == 0)
    {
        const media_server_rep_get_video_play& play = rep._get_video_play();
        if(play.has__nalu() && play.has__pts())
        {
            *pts = play._pts();
            if((_buf_size = play._nalu().size()) > _buf_max_size)
                return -1;

            memcpy(_buf, &*play._nalu().begin(), _buf_size);
            *data = _buf;
            *data_size = _buf_size;
        } else
        {
            *data = 0;
            *data_size = 0;
            *pts = 0;
        }
        return 0;
    }
    return -1;
}

int nn_req::get_video_seek(size_t id, size_t pts) {
    media_server_req req;
    media_server_rep rep;
    media_server_req_get_video_seek* item;

    if((item = req.mutable__get_video_seek()) == 0)
        return -1;
    item->set__session(id);
    item->set__pts(pts);
    req.set__type(cmd_type::get_video);
    req.set__sub_type(cmd_sub_type::seek);

    if(handle(req, rep) < 0)
        return -1;


    if(rep._type() == cmd_type::get_video &&
       rep._sub_type() == cmd_sub_type::seek &&
       rep.has__get_video_seek() &&
       rep._get_video_seek()._status() == 0)
        return 0;
    return -1;
}

int nn_req::get_video_teardown(size_t id) {
    media_server_req req;
    media_server_rep rep;
    media_server_req_get_video_teardown* item;

    if((item = req.mutable__get_video_teardown()) == 0)
        return -1;
    item->set__session(id);
    req.set__type(cmd_type::get_video);
    req.set__sub_type(cmd_sub_type::teardown);

    if(handle(req, rep) < 0)
        return -1;


    if(rep._type() == cmd_type::get_video &&
       rep._sub_type() == cmd_sub_type::teardown &&
       rep.has__get_video_teardown() &&
       rep._get_video_teardown()._status() == 0)
        return 0;
    return -1;
}

nn_req::nn_req(const args_info *args) :
    nn_req(args->_rep_url.c_str())
{
    int send_timeout = 1000; //ms
    int recv_timeout = 1000;

    send_timeout = atoi(args->_req_send_timeout.c_str());
    recv_timeout = atoi(args->_req_recv_timeout.c_str());

    if(set_opt(NN_SOL_SOCKET, NN_RCVTIMEO, &recv_timeout, sizeof(int)) < 0)
        throw;
    if(set_opt(NN_SOL_SOCKET, NN_SNDTIMEO, &send_timeout, sizeof(int)) < 0)
        throw;
}
