//
// Created by rio on 1/31/18.
//

#include "h264_key.h"

h264_key::h264_key(int lab, int dev, int chn)
{
    _key[0] = 0; //reserve
    _key[1] = lab;
    _key[2] = dev;
    _key[3] = chn;
}

bool operator<(const h264_key &l, const h264_key &r) {
    uint32_t l_key, r_key;

    l_key = (l._key[0] << 24) + (l._key[1] << 16) + (l._key[2] << 8) + (l._key[3]);
    r_key = (r._key[0] << 24) + (r._key[1] << 16) + (r._key[2] << 8) + (r._key[3]);

    return l_key < r_key;
}

void h264_key::parse_cache_key(int *lab, int *dev, int *chn) const {
    if(lab)
        *lab = _key[1];
    if(dev)
        *dev = _key[2];
    if(chn)
        *chn = _key[3];
}

h264_key::h264_key(const media_server_pb::video_key &key) :
    h264_key(key._lab(), key._dev(), key._chn())
{
}

int h264_key::serialize_to_arrary(void *data, uint64_t data_size) {
    if(data_size < 0x04)
        return -1;
    memcpy(data, _key, 0x04);
    return 0x04;
}

uint64_t h264_key::key_size() {
    return 0x04;
}

h264_key::h264_key(const h264_key &key)
{
    memcpy(_key, key._key, 0x04);
}
