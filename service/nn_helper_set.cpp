//
// Created by rio on 2/7/18.
//

#include "nn_helper_set.h"

void nn_helper_set::add_helper(nn_helper *helper) {
    _break_loop = 0;
    _helpers.push_back(helper);
    _pfds.push_back(helper->pool_fd());
}

void nn_helper_set::step() {
    int ret;
    if((ret = nn_poll(&*_pfds.begin(), _pfds.size(), 100)) < 0)
    {
        default_log("set", "nn_poll failed", "%s", nn_strerror(errno));
        return;
    }
    for(int i = 0; i < _pfds.size(); ++i)
    {
        _helpers[i]->step(&_pfds[i]);
    }
}

void nn_helper_set::loop(int *break_loop) {
    while(!*break_loop)
    {
        step();
    }
}

void nn_helper_set::loop() {
    while(!_break_loop)
    {
        step();
    }
}

void nn_helper_set::break_loop() {
    _break_loop = 1;
}