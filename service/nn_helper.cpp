//
// Created by rio on 1/30/18.
//

#include "nn_helper.h"
#include "../util/util.h"
#include <nanomsg/reqrep.h>
#include <nanomsg/pubsub.h>

nn_helper::nn_helper(const char *url, int protocol, size_t buf_max_size) {
    int use_bind;
    int ret;

    switch(protocol)
    {
        case NN_PUB:
            use_bind = 1;
            snprintf(_where, sizeof(_where), "%s:%s", "pub", url);
            break;
        case NN_SUB:
            use_bind = 0;
            snprintf(_where, sizeof(_where), "%s:%s", "sub", url);
            break;
        case NN_REQ:
            use_bind = 0;
            snprintf(_where, sizeof(_where), "%s:%s", "req", url);
            break;
        case NN_REP:
            use_bind = 1;
            snprintf(_where, sizeof(_where), "%s:%s", "rep", url);
            break;
    }
    _sock = nn_socket(AF_SP, protocol);
    if(_sock < 0)
    {
        log("nn_socket failed", "%s", nn_strerror(errno));
        throw;
    }

    ret = use_bind ? nn_bind(_sock, url) : nn_connect(_sock, url);
    if(ret < 0)
    {
        use_bind ?
        log("nn_bind failed", "%s", nn_strerror(errno)) :
        log("nn_connect failed", "%s", nn_strerror(errno));
        throw;
    }
    _ep = ret;

    _buf_max_size = buf_max_size;
    _buf = malloc(_buf_max_size);
    if(_buf == 0)
    {
        log("malloc failed", "no mem");
        throw;
    }

    _break_loop = 0;
}

nn_helper::~nn_helper() {
    nn_shutdown(_sock, _ep);
    nn_close(_sock);
    free(_buf);
}

int nn_helper::set_opt(int level, int option, const void *optval, size_t optvallen) {
    int ret;
    if((ret = nn_setsockopt(_sock, level, option, optval, optvallen)) < 0)
    {
        log("set_opt", "%s", nn_strerror(errno));
    }
    return ret;
}

int nn_helper::get_opt(int level, int option, void *optval, size_t *optvallen) {
    int ret;

    if((ret = nn_getsockopt(_sock, level, option, optval, optvallen)) < 0)
    {
        log("set_opt", "%s", nn_strerror(errno));
    }
    return ret;
}

void nn_helper::loop() {
    while(!_break_loop)
    {
        step();
    }
}

void nn_helper::step() {
    log("step unimplement");
    throw;
}

void nn_helper::step(nn_pollfd *fd) {
    log("step unimplement");
    throw;
}

int nn_helper::send(const void *buf, size_t buf_size) {
    int ret;

    if((ret = nn_send(_sock, buf, buf_size, 0)) < 0)
    {
        log("nn_send failed", "%s", nn_strerror(errno));
    } else
    {
        //log("nn_send ok", "size=%d", buf_size);
    }
    return ret;
}

int nn_helper::send() {
    return send(_buf, _buf_size);
}

int nn_helper::recv(void *buf, size_t buf_size) {
    int ret;

    if((ret = nn_recv(_sock, buf, buf_size, 0)) < 0)
    {
        log("nn_recv failed", "%s", nn_strerror(errno));
    } else
    {
        //log("nn_recv ok", "size=%d", ret);
    }
    return ret;
}

int nn_helper::recv() {
    int ret;

    if((ret = recv(_buf, _buf_max_size)) > 0)
    {
        _buf_size = ret;
    }
    return ret;
}

nn_pollfd nn_helper::pool_fd() {
    return nn_pollfd{ .fd = _sock, .events = NN_POLLIN | NN_POLLOUT };
}

void nn_helper::log(const char *what, const char *fmt, ...) {
    va_list va;
    va_start(va, fmt);
    default_vlog(_where, what, fmt, va);
    va_end(va);
}

void nn_helper::break_loop() {
    _break_loop = 1;
}
