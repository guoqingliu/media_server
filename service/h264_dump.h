//
// Created by rio on 2/2/18.
//

#ifndef MEDIA_SERVER_H264_CACHE_DUMP_H
#define MEDIA_SERVER_H264_CACHE_DUMP_H

#include <unistd.h>
#include <sys/stat.h>
#include <hdfs.h>
#include <thread>
#include "../util/util.h"
#include "h264_key.h"
#include "../util/thread_safe_queue.h"
#include "../cache/h264_cache_reader.h"

#define AVIO_BUFF_SIZE (400 * 1024)
#define H264_CACHE_DUMP_DEFAULT_SIZE (1 * 1024 * 1024)
class h264_dump
{
public:
    h264_dump(h264_key key, const char* repos,
                    thread_safe_queue<std::string>* upload_to_hdfs,
                    const AVCodecParameters* codec_params);
    ~h264_dump();
    int init_dump(size_t pts);
    int term_dump();
    int check_cp_set();
    int step(const void* data, size_t data_size, size_t pts);

private:
    void codec_parameters_init();
    static void make_dir(const char* path, mode_t mode = S_IRWXU | S_IRWXG);
    int gen_out_file_name(char* file, size_t size, uint64_t pts);
    int check_pts();
    int get_hour_by_pts(uint64_t pts);
    static void log(const char* what, const char* fmt = 0, ...);
private:
    h264_key _cache_key;
    std::vector<uint8_t> _sps;
    std::vector<uint8_t> _pps;
    void* _buf;
    size_t _buf_size;
    thread_safe_queue<std::string>* _upload_to_hdfs;
    AVCodecParameters _codec_params;
    AVFormatContext* _fmt_ctx;
    size_t _begin_pts;
    size_t _end_pts;
    AVPacket _pkt;
    char _repos[0x200];
    char _err_buf[AV_ERROR_MAX_STRING_SIZE];
    h264_cache_reader* _cache_reader;
    int _cp_set_flag;
};

class h264_cache_dump_map
{
    typedef std::map<h264_key, h264_dump*> dump_map_t;
public:
    /**
     * @param hdfs_repos hdfs://192.168.1.222:8929/file/611/mp4
     * @param local_repos /home/rio/video/611/mp4
     */
    h264_cache_dump_map(const char* hdfs_repos, const char* local_repos);
    ~h264_cache_dump_map();
    h264_dump* add_or_get_cache_dump(h264_key key);
    h264_dump* add_cache_dump(h264_key key);
    h264_dump* get_cache_dump(h264_key key);
    int set_default_codec_params(const char* sample_file_fmt);
    int set_default_codec_params(int chn, const AVCodecParameters* codec_params);
    static void log(const char *what, const char *fmt = 0, ...);
    static void upload_to_hdfs(h264_cache_dump_map* map);
private:
    int _dcp_set_flag;
    AVCodecParameters _default_codec_params[0x04];
    dump_map_t _dump_map;
    hdfsFS _fs;
    int _break_loop;
    char _hdfs_repos[0x200];
    char _local_repos[0x200];
    thread_safe_queue<std::string> _upload_to_hdfs;
    std::thread _upload_to_hdfs_task;
};
#endif //MEDIA_SERVER_H264_CACHE_DUMP_H
