//
// Created by rio on 2/7/18.
//

#ifndef MEDIA_SERVER_NN_SUB_H
#define MEDIA_SERVER_NN_SUB_H


#include "nn_helper.h"

class nn_sub : public nn_helper
{
public:
    nn_sub(const char* url);
    nn_sub(const args_info* args);
    int sub(const void* topic, size_t topic_size);
    int unsub(const void* topic, size_t topic_size);
    int get_sub(void* topic, size_t* topic_size);
    template<typename _Callback>
    int step(_Callback callback)
    {

        if(recv() < 0)
            return -1;
        ++_count;
        int topic_size = h264_key::key_size();
        if(_pub.ParseFromArray((uint8_t*)_buf + topic_size, _buf_size - topic_size))
        {
            const video& pub_video = _pub._video();
            for(int i = 0; i < pub_video._items_size(); ++i)
            {
                const video_item& item = pub_video._items(i);
                h264_key key(item._key());
                size_t pts = item._pts();
                callback(key, (const uint8_t*)&*item._nalu().begin(), item._nalu().size(), pts);
            }
            return 0;
        } else
        {
            log("ParseFromArray failed");
            return -1;
        }
    }
    void dump()
    {
        log("dump", "count=%zu", _count);
    }
private:
    size_t _count;
    media_server_pub _pub;
};
#endif //MEDIA_SERVER_NN_SUB_H
