//
// Created by rio on 2/2/18.
//

#include "h264_dump.h"
#include <hdfs.h>

h264_dump::h264_dump(h264_key key, const char* repos,
                                 thread_safe_queue<std::string>* upload_to_hdfs,
                                 const AVCodecParameters *codec_params) :
       _fmt_ctx(0), _begin_pts(0), _end_pts(0), _cache_key(key),
       _upload_to_hdfs(upload_to_hdfs), _cache_reader(0)
{
    codec_parameters_init();
    log("h264_dump", "codec_params=%p", codec_params);
    _cp_set_flag = (!codec_params || avcodec_parameters_copy(&_codec_params, codec_params) < 0) ? 0 : 1;
    log("h264_dump", "_cp_set_flag=%d", _cp_set_flag);
    strcpy(_repos, repos);
    _buf_size = H264_CACHE_DUMP_DEFAULT_SIZE;
    if((_buf = av_malloc(_buf_size)) == 0)
    {
        log("h264_dump", "av_malloc %zu failed", _buf_size);
        throw;
    }
}

h264_dump::~h264_dump() {
    if(_fmt_ctx)
        term_dump();
}

int h264_dump::init_dump(size_t pts) {
    char dump_file[0x400];
    AVStream* out_stream;

    if(gen_out_file_name(dump_file, sizeof(dump_file), pts) < 0)
        return -1;

    avformat_alloc_output_context2(&_fmt_ctx, NULL, NULL, dump_file);
    if (!_fmt_ctx) {
        log("avformat_alloc_output_context2 failed");
        goto failed;
    }

    if((out_stream = avformat_new_stream(_fmt_ctx, NULL)) == 0)
    {
        log("avformat_new_stream failed");
        goto failed;
    }
    out_stream->time_base = AVRational{1, TIME_BASE};
    if(avcodec_parameters_copy(out_stream->codecpar, &_codec_params) < 0)
    {
        log("avcodec_parameters_copy failed");
        goto failed;
    }
    out_stream->codecpar->codec_tag = 0;
    if (!(_fmt_ctx->flags & AVFMT_NOFILE)) {
        if(avio_open(&_fmt_ctx->pb, dump_file, AVIO_FLAG_WRITE) < 0)
        {
            log("avio_open failed");
            goto failed;
        }
    }
    if(avformat_write_header(_fmt_ctx, 0) < 0)
    {
        log("avformat_write_header failed");
        goto failed;
    }
    _begin_pts = _end_pts = pts;
    return 0;

    failed:
    if (_fmt_ctx && !(_fmt_ctx->flags & AVFMT_NOFILE))
        avio_closep(&_fmt_ctx->pb);
    avformat_free_context(_fmt_ctx);
    return -1;
}

int h264_dump::term_dump() {
    char src_file[0x400];
    char des_file[0x400];
    char* pos;

    if(av_write_trailer(_fmt_ctx) < 0)
    {
        log("av_write_trailer failed");
        return -1;
    }

    strcpy(src_file, _fmt_ctx->filename);
    strcpy(des_file, _fmt_ctx->filename);

    if (_fmt_ctx && !(_fmt_ctx->flags & AVFMT_NOFILE))
        avio_closep(&_fmt_ctx->pb);
    avformat_free_context(_fmt_ctx);
    _fmt_ctx = 0;

    pos = strstr(des_file, ".mp4");
    sprintf(pos, "_%zu.mp4", _end_pts);

    if(rename(src_file, des_file) < 0)
    {
        log("rename failed");
        return -1;
    }
    _upload_to_hdfs->push(std::string{des_file});
    return 0;
}

int h264_dump::step(const void* data, size_t data_size, size_t pts) {
    static uint8_t _g_spe[] = {0, 0, 0, 1};
    int ret;
    uint8_t nalu_type;

    if(!data || !data_size)
        return -1;

    if(check_cp_set() < 0)
    {
        _cache_reader->write(data, data_size, pts);
        return 0;
    }

    nalu_type = *(uint8_t*)data & 0x1f;

    switch(nalu_type)
    {
        case 0x07:
            if(!_sps.size())
            {
                _sps.resize(data_size + sizeof(_g_spe));
                memcpy(const_cast<uint8_t*>(&*_sps.begin()), _g_spe, sizeof(_g_spe));
                memcpy(const_cast<uint8_t*>(&*_sps.begin()) + sizeof(_g_spe), data, data_size);
            }
            break;
        case 0x08:
            if(!_pps.size())
            {
                _pps.resize(data_size + sizeof(_g_spe));
                memcpy(const_cast<uint8_t*>(&*_pps.begin()), _g_spe, sizeof(_g_spe));
                memcpy(const_cast<uint8_t*>(&*_pps.begin()) + sizeof(_g_spe), data, data_size);
            }
            break;
        default:
            if(!_pps.size() || !_sps.size() ||
                    _buf_size < _sps.size() + _pps.size() + sizeof(_g_spe) + data_size)
                return -1;
            memcpy(_buf, &*_sps.begin(), _sps.size());
            memcpy((uint8_t*)_buf + _sps.size(), &*_pps.begin(), _pps.size());
            memcpy((uint8_t*)_buf + _sps.size() + _pps.size(),
                _g_spe, sizeof(_g_spe));
            memcpy((uint8_t*)_buf + _sps.size() + _pps.size() + sizeof(_g_spe),
                data, data_size);

            av_init_packet(&_pkt);
            _pkt.stream_index = 0;
            _pkt.data = (uint8_t*)_buf;
            _pkt.size = _sps.size() + _pps.size() + sizeof(_g_spe) + data_size;
            _pkt.pts = _pkt.dts = pts;
            _pkt.duration = 0;
            _pkt.pos = -1;
            if(check_pts() < 0)
                return 0;

            /* copy packet */

            _pkt.pts = _pkt.dts = av_rescale_q_rnd(_pkt.pts, AVRational{1, TIME_US_BASE}, _fmt_ctx->streams[0]->time_base,
                                                   (enum AVRounding)(AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX));
            //log_packet(&_pkt);
            ret = av_interleaved_write_frame(_fmt_ctx, &_pkt);
            if (ret < 0) {
                log("av_interleaved_write_frame failed");
                return -1;
            }
            break;
    }
    return 0;
}

void h264_dump::codec_parameters_init() {
    memset(&_codec_params, 0, sizeof(AVCodecParameters));

    _codec_params.codec_type          = AVMEDIA_TYPE_UNKNOWN;
    _codec_params.codec_id            = AV_CODEC_ID_NONE;
    _codec_params.format              = -1;
    _codec_params.field_order         = AV_FIELD_UNKNOWN;
    _codec_params.color_range         = AVCOL_RANGE_UNSPECIFIED;
    _codec_params.color_primaries     = AVCOL_PRI_UNSPECIFIED;
    _codec_params.color_trc           = AVCOL_TRC_UNSPECIFIED;
    _codec_params.color_space         = AVCOL_SPC_UNSPECIFIED;
    _codec_params.chroma_location     = AVCHROMA_LOC_UNSPECIFIED;
    _codec_params.sample_aspect_ratio = (AVRational){ 0, 1 };
    _codec_params.profile             = FF_PROFILE_UNKNOWN;
    _codec_params.level               = FF_LEVEL_UNKNOWN;
}

void h264_dump::make_dir(const char *path, mode_t mode) {

    char dir[0x100];
    char* pos;

    // /base/y/m/d
    strcpy(dir, path);
    pos = dir;
    do
    {
        ++pos;

        if((pos = strstr(pos, "/")) == 0)
        {
            if(access(dir, F_OK) < 0)
                mkdir(dir, mode);
        } else
        {
            *pos = 0;
            if(access(dir, F_OK) < 0)
                mkdir(dir, mode);
            *pos = '/';
        }

    }while(pos);
}

int h264_dump::gen_out_file_name(char *file, size_t size, uint64_t pts) {
    int pos;
    time_t time;
    tm tm;
    int lab, dev, chn;

    pos = 0;
    pos += snprintf(file, size, _repos);

    _cache_key.parse_cache_key(&lab, &dev, &chn);
    pos += snprintf(file + pos, size - pos, "/%03d%03d%03d", lab, dev, chn);

    parse_pts(pts, &time);
    localtime_r(&time, &tm);

    pos += snprintf(file + pos, size - pos, "/%04d/%02d/%02d",
                    tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday);
    make_dir(file);
    pos += snprintf(file + pos, size - pos, "/%zu.mp4", pts);
    return pos;
}

int h264_dump::check_pts() {
    if(_pkt.pts == AV_NOPTS_VALUE)
        return _fmt_ctx == 0 ? -1 : 0;
    if(_fmt_ctx == 0)
    {
        if(init_dump(_pkt.pts) < 0)
            return -1;
        return 0;
    }

    if(get_hour_by_pts(_pkt.pts) != get_hour_by_pts(_begin_pts))
    {
        if(term_dump() < 0)
            return 0;
        if(init_dump(_pkt.pts) < 0)
            return -1;
        return 0;
    } else
    {
        _end_pts = _pkt.pts;
        return 0;
    }
}

int h264_dump::get_hour_by_pts(uint64_t pts) {
    time_t time;
    tm tm;

    parse_pts(pts, &time);
    localtime_r(&time, &tm);
    return tm.tm_hour;
}

void h264_dump::log(const char *what, const char *fmt, ...) {
    va_list va;
    va_start(va, fmt);
    default_vlog("h264_dump", what, fmt, va);
    va_end(va);
}

int h264_dump::check_cp_set() {
   if(_cp_set_flag)
       return 0;

    if(_cache_reader == 0)
    {
        _cache_reader = new h264_cache_reader(4 * 1024 * 1024);
    }

    if(_cache_reader->get_size() > 2 * 1024 * 1024)
    {
        _cache_reader->reset();
        if(_cache_reader->init_code_parameters_by_cache(&_codec_params) == 0)
        {
            delete _cache_reader;
            _cache_reader = 0;
            _cp_set_flag = 1;
            log("check_cp_set", "init_code_parameters_by_cache ok");
            return 0;
        }
    }
    return -1;
}

h264_cache_dump_map::h264_cache_dump_map(const char* hdfs_repos, const char* local_repos) :
    _break_loop(0), _dcp_set_flag(0),
    _upload_to_hdfs_task(&h264_cache_dump_map::upload_to_hdfs, this)
{
    return;
    char hdfs_repos_copy[0x200];
    char* host;
    char* port;
    char* repos;
    uint16_t num_port;

    strcpy(hdfs_repos_copy, hdfs_repos);

    host = strstr(hdfs_repos_copy, "hdfs://");
    host += 7;
    port = strstr(hdfs_repos_copy, ":");
    *port = 0;
    ++port;
    repos = strstr(port, "/");
    *repos = 0;
    num_port = atoi(port);
    *repos = '/';

    if((_fs = hdfsConnect(host, num_port)) == 0)
        throw;
    strcpy(_hdfs_repos, repos);
    strcpy(_local_repos, local_repos);
}

h264_cache_dump_map::~h264_cache_dump_map() {
    dump_map_t::iterator it;
    for(it = _dump_map.begin(); it != _dump_map.end(); ++it)
        delete it->second;
    _break_loop = 1;
    _upload_to_hdfs_task.join();
    if(hdfsDisconnect(_fs) < 0)
        throw;
}

h264_dump *h264_cache_dump_map::add_or_get_cache_dump(h264_key key) {
    h264_dump* dump;

    if(dump = get_cache_dump(key))
        return dump;
    return add_cache_dump(key);
}

h264_dump *h264_cache_dump_map::add_cache_dump(h264_key key) {
    dump_map_t::iterator it;
    h264_dump* dump;
    int chn;
    key.parse_cache_key(0, 0, &chn);
    if(chn >= 0x04)
        return 0;
    if((dump = new h264_dump(key, _local_repos, &_upload_to_hdfs,
                             _dcp_set_flag ? &_default_codec_params[chn] : 0)) == 0)
        return dump;
    _dump_map[key] = dump;
    return dump;
}

h264_dump *h264_cache_dump_map::get_cache_dump(h264_key key) {
    dump_map_t::iterator it;
    if((it = _dump_map.find(key)) != _dump_map.end())
        return it->second;
    return 0;
}

int h264_cache_dump_map::set_default_codec_params(const char *sample_file_fmt) {
    AVFormatContext* fmt_ctx;
    char err_buf[AV_ERROR_MAX_STRING_SIZE];
    char sample_file[0x200];
    int ret;

    if(sample_file_fmt == 0 || *sample_file_fmt == 0)
    {
        _dcp_set_flag = 0;
        return -1;
    }

    for(int chn = 0; chn < 0x04; ++chn)
    {
        fmt_ctx = 0;
        snprintf(sample_file, sizeof(sample_file), sample_file_fmt, chn);
        if ((ret = avformat_open_input(&fmt_ctx, sample_file, 0, 0)) < 0) {
            av_make_error_string(err_buf, sizeof(err_buf), ret);
            log("set_default_codec_params", "avformat_open_input %s failed %s", sample_file, err_buf);
            goto failed;
        }
        if ((ret = avformat_find_stream_info(fmt_ctx, 0)) < 0) {
            av_make_error_string(err_buf, sizeof(err_buf), ret);
            log("set_default_codec_params", "avformat_find_stream_info %s failed %s", sample_file, err_buf);
            goto failed;
        }

        if(fmt_ctx->nb_streams == 0 || fmt_ctx->streams[0]->codecpar->codec_type != AVMEDIA_TYPE_VIDEO)
        {
            log("set_default_codec_params", "%s streams[0] not video stream", sample_file);
            goto failed;
        }
        if(set_default_codec_params(chn, fmt_ctx->streams[0]->codecpar) < 0)
        {
            log("set_default_codec_params", "avcodec_parameters_copy failed");
            goto failed;
        }
        avformat_close_input(&fmt_ctx);
        log("set_default_codec_params", "%s ok", sample_file);
    }
    _dcp_set_flag = 1;
    return 0;
failed:
    avformat_close_input(&fmt_ctx);
    _dcp_set_flag = 0;
    return -1;
}

int h264_cache_dump_map::set_default_codec_params(int chn, const AVCodecParameters *codec_params) {
    memset(&_default_codec_params[chn], 0, sizeof(AVCodecParameters));

    _default_codec_params[chn].codec_type          = AVMEDIA_TYPE_UNKNOWN;
    _default_codec_params[chn].codec_id            = AV_CODEC_ID_NONE;
    _default_codec_params[chn].format              = -1;
    _default_codec_params[chn].field_order         = AV_FIELD_UNKNOWN;
    _default_codec_params[chn].color_range         = AVCOL_RANGE_UNSPECIFIED;
    _default_codec_params[chn].color_primaries     = AVCOL_PRI_UNSPECIFIED;
    _default_codec_params[chn].color_trc           = AVCOL_TRC_UNSPECIFIED;
    _default_codec_params[chn].color_space         = AVCOL_SPC_UNSPECIFIED;
    _default_codec_params[chn].chroma_location     = AVCHROMA_LOC_UNSPECIFIED;
    _default_codec_params[chn].sample_aspect_ratio = (AVRational){ 0, 1 };
    _default_codec_params[chn].profile             = FF_PROFILE_UNKNOWN;
    _default_codec_params[chn].level               = FF_LEVEL_UNKNOWN;

    return avcodec_parameters_copy(&_default_codec_params[chn], codec_params);
}

void h264_cache_dump_map::log(const char *what, const char *fmt, ...) {
    va_list va;
    va_start(va, fmt);
    default_vlog("h264_cache_dump_map", what, fmt, va);
    va_end(va);
}

void h264_cache_dump_map::upload_to_hdfs(h264_cache_dump_map *map) {
    std::string local_file;
    std::string remote_file;
    const char* rel_path;

    while(!map->_break_loop)
    {
        if(map->_upload_to_hdfs.try_pop(local_file, 500))
        {
            uint8_t* data;
            size_t data_size;
            size_t upload_size;
            hdfsFile file;
            int ret;

            if(av_file_map(local_file.c_str(), &data, &data_size, 0, 0) < 0)
            {
                log("upload_to_hdfs", "av_file_map %s failed", local_file.c_str());
            }
            else
            {
                upload_size = 0;
                rel_path = local_file.c_str() + strlen(map->_local_repos);
                remote_file = map->_hdfs_repos;
                remote_file += rel_path;

                if((file = hdfsOpenFile(map->_fs, remote_file.c_str(),
                                        O_WRONLY | O_CREAT, 0, 0, 0)) == 0)
                {
                    log("upload_to_hdfs", "hdfsOpenFile %s failed", remote_file.c_str());
                }
                else
                {
                    while(upload_size < data_size)
                    {
                        ret = hdfsWrite(map->_fs, file, data + upload_size, data_size - upload_size);

                        if(ret < 0)
                        {
                            log("upload_to_hdfs", "hdfsWrite %s failed", remote_file.c_str());
                            break;
                        }
                        upload_size += ret;
                    }
                    hdfsCloseFile(map->_fs, file);
                }

                av_file_unmap(data, data_size);

                if(upload_size == data_size)
                {
                    log("upload_to_hdfs", "%s to %s ok", local_file.c_str(), remote_file.c_str());
                    if(remove(local_file.c_str()) == 0)
                    {
                        log("upload_to_hdfs", "remove %s ok", local_file.c_str());
                    } else
                    {
                        log("upload_to_hdfs", "remove %s failed", local_file.c_str());
                    }
                } else
                {
                    log("upload_to_hdfs", "%s to %s failed", local_file.c_str(), remote_file.c_str());
                }
            }
        }
    }
}
