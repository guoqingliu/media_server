//
// Created by rio on 4/26/18.
//

#ifndef MEDIA_SERVER_H264_PARSE_H
#define MEDIA_SERVER_H264_PARSE_H

#include <cstdint>
#include <cstddef>
#include <thread>
#include <vector>
#include "../util/util.h"

class h264_parse
{
public:
    h264_parse(uint8_t* buf, size_t buf_size);

    int step_impl(uint8_t* unit, size_t unit_size);
    int step(uint8_t* unit, size_t unit_size)
    {
        switch(_step_cnts % 25)
        {
            case 0: //sps
                if(unit_size < _sps.size())
                    return -1;
                memcpy(unit, &*_sps.begin(), _sps.size());
                ++_step_cnts;
                return _sps.size();
            case 1: //pps
                if(unit_size < _pps.size())
                    return -1;
                memcpy(unit, &*_pps.begin(), _pps.size());
                ++_step_cnts;
                return _pps.size();
            default:
                ++_step_cnts;
                return step_impl(unit, unit_size);
        }
    }
private:
    uint8_t* _buf_begin;
    uint8_t* _buf_end;
    uint8_t* _pos;
    size_t _step_cnts;
    std::vector<uint8_t> _sps;
    std::vector<uint8_t> _pps;
    static uint8_t _g_spe[0x03];
};

class h264_ffmpeg_parse
{
public:
    h264_ffmpeg_parse(const char* file) : _file(file), _fmt(0), _step_cnts(0)
    {
        av_register_all();
    }


    int step(uint8_t* unit, size_t unit_size)
    {
        switch(_step_cnts % 25)
        {
            case 0: //sps
                if(unit_size < _sps.size())
                    return -1;
                memcpy(unit, &*_sps.begin(), _sps.size());
                ++_step_cnts;
                return _sps.size();
            case 1: //pps
                if(unit_size < _pps.size())
                    return -1;
                memcpy(unit, &*_pps.begin(), _pps.size());
                ++_step_cnts;
                return _pps.size();
            default:
                ++_step_cnts;
                return step_impl(unit, unit_size);
        }
    }

    int step_impl(uint8_t* unit, size_t unit_size)
    {
        AVPacket pkt;
        int pos;

        if(_fmt == 0 && init() < 0)
            return -1;

        av_init_packet(&pkt);

        if(av_read_frame(_fmt, &pkt) < 0)
        {
            term();
            if(init() < 0)
                return -1;
            return step_impl(unit, unit_size);
        }
        else
        {
            pos = *(pkt.data + 0x02) == 0 ? 0x04 : 0x03;
            if(unit_size < pkt.size - pos)
                return 0;
            memcpy(unit, pkt.data + pos, pkt.size - pos);
            print_buf(pkt.data + pos, pkt.size - pos);
            return pkt.size - pos;
        }
    }

    int init()
    {
        _fmt = 0;
        if(avformat_open_input(&_fmt, _file.c_str(), 0, 0) < 0)
            return -1;
        if(avformat_find_stream_info(_fmt, 0) < 0)
        {
            term();
            return -1;
        }
        return 0;
    }

    void term()
    {
        if(_fmt)
            avformat_close_input(&_fmt);
    }

private:
    std::vector<uint8_t> _sps;
    std::vector<uint8_t> _pps;
    std::string _file;
    AVFormatContext* _fmt;
    size_t _step_cnts;
};

class h264_file_parse
{
public:
    h264_file_parse(const char* file)
    {
        if(av_file_map(file, &_data, &_data_size, 0, 0) < 0)
            throw;
        _parse = new h264_parse(_data, _data_size);
    }
    int step(uint8_t* unit, size_t unit_size)
    {
        int ret;
        ret = _parse->step(unit, unit_size);
        //print_buf(unit, ret);
        return ret;
    }
    ~h264_file_parse()
    {
        delete _parse;
        av_file_unmap(_data, _data_size);
    }
private:
    uint8_t* _data;
    size_t _data_size;
    h264_parse* _parse;
};

inline void h264_parse_test()
{
    uint8_t data[] = {
            0, 0, 0, 1, 2,
            0, 0, 0, 1, 2, 3, 4,
               0, 0, 1, 2, 3, 4, 5, 6,
            0, 0, 0, 1, 2, 3, 4, 5, 6, 7, 8,
            0, 0, 0, 1, 2, 3, 4, 5, 6,
               0, 0, 1, 2, 3, 4,
            0, 0, 0, 1, 2
    };
    size_t data_size = sizeof(data);

    uint8_t unit[200];
    size_t unit_size = sizeof(unit);
    h264_parse parse(data, data_size);

    while(true)
    {
        int size = parse.step(unit, unit_size);
        print_buf(unit, size);
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
    }
}
#endif //MEDIA_SERVER_H264_PARSE_H
