//
// Created by rio on 2/7/18.
//

#ifndef MEDIA_SERVER_NN_REP_H
#define MEDIA_SERVER_NN_REP_H

#include "nn_helper.h"
#include "../session/h264_session_map.h"
#include "nn_pub.h"

class nn_rep : public nn_helper
{
public:
    nn_rep(const char *rep_url, const char* hdfs_repos, const char* local_repos,
           const char* pub_url, const char* sample_h264);    //sample_h264 file using setting codec paramters
    nn_rep(const args_info* args);
    void step() override;
    void step(nn_pollfd* pfd) override;
    nn_pollfd pool_fd() override;
    void handle_push_video(const media_server_req& req, media_server_rep& rep);
    void handle_get_avl_video(const media_server_req& req, media_server_rep& rep);
    void handle_get_video_setup(const media_server_req& req, media_server_rep& rep);
    void handle_get_video_play(const media_server_req& req, media_server_rep& rep);
    void handle_get_video_seek(const media_server_req& req, media_server_rep& rep);
    void handle_get_video_teardown(const media_server_req& req, media_server_rep& rep);
    void handle_cmd_not_found(const media_server_req& req, media_server_rep& rep);
    void dump()
    {
        log("dump", "push_video_count=%zu", _push_video_count);
    }
private:
    size_t _push_video_count;
    h264_session_map _session_map;
    h264_cache_dump_map _dump_map;
    nn_pub _pub;
};
#endif //MEDIA_SERVER_NN_REP_H
