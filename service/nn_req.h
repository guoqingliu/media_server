//
// Created by rio on 2/7/18.
//

#ifndef MEDIA_SERVER_NN_REQ_H
#define MEDIA_SERVER_NN_REQ_H


#include "nn_helper.h"

class nn_req : public nn_helper
{
public:
    nn_req(const char* url);
    nn_req(const args_info* args);
    nn_pollfd pool_fd() override;
    void push_h264_file(const char* file_fmt);
    void request(media_server_req& req);
    int handle(const media_server_req& req, media_server_rep& rep);
    int push_video(char lab, char dev, char chn, void* data, size_t data_size);
    int get_video_setup(size_t* id, char lab, char dev, char chn, size_t begin_pts, size_t end_pts);
    int get_video_play(size_t id, void** data, size_t* data_size, size_t* pts);
    int get_video_seek(size_t id, size_t pts);
    int get_video_teardown(size_t id);

private:
};
#endif //MEDIA_SERVER_NN_REQ_H
