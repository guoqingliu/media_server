//
// Created by rio on 2/7/18.
//

#ifndef MEDIA_SERVER_NN_PUB_H
#define MEDIA_SERVER_NN_PUB_H

#include "nn_helper.h"

class nn_pub : public nn_helper
{
public:
    nn_pub(const char *url);
    nn_pub(const args_info* args);
    ~nn_pub();
    void publish(h264_key key, const void* nalu, size_t nalu_size, size_t pts);
    void step() override;
    void step(nn_pollfd* fd) override;
    nn_pollfd pool_fd() override;
    void dump()
    {
        log("dump", "pub_count=%zu", _count);
    }

private:
    int check_nalu(const uint8_t* nalu, size_t nalu_size)
    {
        static uint8_t _g_spe[] = {0, 0, 0, 1};
        int nalu_type;

        nalu_type = (*nalu) & 0x1f;

        switch(nalu_type)
        {
            case 0x07:
                _sps.resize(nalu_size + sizeof(_g_spe));
                memcpy(const_cast<uint8_t*>(&*_sps.begin()), _g_spe, sizeof(_g_spe));
                memcpy(const_cast<uint8_t*>(&*_sps.begin() + sizeof(_g_spe)), nalu, nalu_size);
                return 0;
            case 0x08:
                _pps.resize(nalu_size + sizeof(_g_spe));
                memcpy(const_cast<uint8_t*>(&*_pps.begin()), _g_spe, sizeof(_g_spe));
                memcpy(const_cast<uint8_t*>(&*_pps.begin() + sizeof(_g_spe)), nalu, nalu_size);
                return 0;
            default:
                if(_sps.size() == 0 || _pps.size() == 0 ||
                        _buf_max_size < _sps.size() + _pps.size() + nalu_size + sizeof(_g_spe))
                    return -1;
                return nalu_size;
        }
    }
private:
    size_t _count;
    media_server_pub _pub;
    std::vector<uint8_t> _sps;
    std::vector<uint8_t> _pps;
};
#endif //MEDIA_SERVER_NN_PUB_H
