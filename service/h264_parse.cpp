//
// Created by rio on 4/26/18.
//

#include "h264_parse.h"

uint8_t h264_parse::_g_spe[] = {0, 0, 1};

h264_parse::h264_parse(uint8_t *buf, size_t buf_size) : _buf_begin(buf), _buf_end(buf + buf_size)
{
    _step_cnts = 0;
    _pos = 0;
    _sps.resize(0x40);
    _pps.resize(0x10);

    int ret;

    if((ret = step_impl(const_cast<uint8_t*>(&*_sps.begin()), _sps.size())) < 0 &&
            (*_sps.begin() & 0x1f) != 0x07)
        throw;
    _sps.resize(ret);

    if((ret = step_impl(const_cast<uint8_t*>(&*_pps.begin()), _pps.size())) < 0 &&
            (*_pps.begin() & 0x1f) != 0x08)
        throw;
    _pps.resize(ret);
}

int h264_parse::step_impl(uint8_t *unit, size_t unit_size) {

    uint8_t* begin,* end;
    uint8_t* r_begin,* r_end;

    if(_pos == 0)
    {
        for(_pos = _buf_begin; _pos < _buf_end - sizeof(_g_spe); ++_pos)
        {
            if(memcmp(_pos, _g_spe, sizeof(_g_spe)) == 0)
            {
                break;
            }
        }
        if(_pos >= _buf_end - sizeof(_g_spe))
            return -1;
    }

    begin = _pos;

    for(end = begin + sizeof(_g_spe); end < _buf_end - sizeof(_g_spe); ++end)
    {
        if(memcmp(end, _g_spe, sizeof(_g_spe)) == 0)
        {
            _pos = end;
            break;
        }
    }

    if(end >= _buf_end - sizeof(_g_spe))
    {
        r_begin = begin + sizeof(_g_spe);
        r_end = _buf_end;

        if(r_end - r_begin > unit_size)
        {
            //log
            return -1;
        }
        memcpy(unit, r_begin, r_end - r_begin);
        _pos = 0;
        return r_end - r_begin;
    }
    else
    {
        r_begin = begin + sizeof(_g_spe);
        r_end = *(end - 1) == 0 ? (end - 1) : end;
        if(r_end - r_begin > unit_size)
        {
            //log
            return -1;
        }
        memcpy(unit, r_begin, r_end - r_begin);
        return r_end - r_begin;
    }
}
