//
// Created by rio on 2/7/18.
//

#include <nanomsg/pubsub.h>
#include "nn_pub.h"

nn_pub::nn_pub(const char *url) :
        nn_helper(url, NN_PUB), _count(0) {
}

void nn_pub::step()
{
    throw;
}

void nn_pub::step(nn_pollfd *fd) {
    throw;
}

nn_pub::~nn_pub() {
}

nn_pollfd nn_pub::pool_fd() {
    throw;
}

void nn_pub::publish(h264_key key, const void *nalu, size_t nalu_size, size_t pts) {
    int lab, dev, chn;
    size_t size;
    static uint8_t _g_spe[] = {0, 0, 0, 1};
    int topic_size;

    video* pub_video = _pub.mutable__video();
    pub_video->clear__items();
    key.parse_cache_key(&lab, &dev, &chn);
    video_item* video_item = pub_video->add__items();
    video_key* video_key = video_item->mutable__key();
    video_key->set__lab(lab);
    video_key->set__dev(dev);
    video_key->set__chn(chn);
    video_item->set__pts(pts);

    if(_buf_max_size <  sizeof(_g_spe) + nalu_size)
        return;
    memcpy((uint8_t*)_buf, _g_spe, sizeof(_g_spe));
    memcpy((uint8_t*)_buf + sizeof(_g_spe), nalu, nalu_size);
    video_item->set__nalu((uint8_t*)_buf, _sps.size() + _pps.size() + sizeof(_g_spe) + nalu_size);

    topic_size = key.serialize_to_arrary(_buf, _buf_max_size);

    if((size = _pub.ByteSize()) < _buf_max_size - topic_size)
    {
        if(_pub.SerializeToArray((uint8_t*)_buf + topic_size, _buf_max_size - topic_size))
        {
            send(_buf, size + topic_size);
            ++_count;
        } else
        {
            log("SerializeToArray failed");
        }
    } else
    {
        log("SerializeToArray too many bytes");
    }
}

nn_pub::nn_pub(const args_info *args) :
    nn_pub(args->_pub_url.c_str())
{
    int send_buf_size;

    send_buf_size = 4 * 1024 * 1024;
    if(set_opt(NN_SOL_SOCKET, NN_SNDBUF, &send_buf_size, sizeof(int)) < 0)
        throw;
}
