//
// Created by rio on 2/7/18.
//

#ifndef MEDIA_SERVER_NN_HELPER_SET_H
#define MEDIA_SERVER_NN_HELPER_SET_H

#include "nn_helper.h"

class nn_helper_set
{
public:
    void add_helper(nn_helper* helper);
    void step();
    void loop(int* break_loop);
    void loop();
    void break_loop();

private:
    std::vector<nn_helper*> _helpers;
    std::vector<nn_pollfd> _pfds;
    int _break_loop;
};


#endif //MEDIA_SERVER_NN_HELPER_SET_H
