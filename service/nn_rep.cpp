//
// Created by rio on 2/7/18.
//

#include <nanomsg/reqrep.h>
#include "nn_rep.h"

nn_rep::nn_rep(const char *rep_url, const char* hdfs_repos, const char* local_repos,
               const char* pub_url, const char* sample_h264_fmt) :
        nn_helper(rep_url, NN_REP), _pub(pub_url), _dump_map(hdfs_repos, local_repos),
        _session_map(/*repos*/ hdfs_repos), _push_video_count(0){
    _dump_map.set_default_codec_params(sample_h264_fmt);
}

void nn_rep::step() {
    if(recv() < 0)
        return;

    int ret;
    media_server_req req;
    media_server_rep rep;
    if(req.ParseFromArray(_buf, _buf_size))
    {
        switch(req._type())
        {
            case cmd_type::push_video:          //Sharing layer client: push_vedio
                handle_push_video(req, rep);
                ++_push_video_count;
                break;
            case cmd_type::get_avl_video:
                handle_get_avl_video(req, rep);
                break;
            case cmd_type::get_video:
                switch(req._sub_type())
                {
                    case cmd_sub_type::setup:
                        handle_get_video_setup(req, rep);
                        break;
                    case cmd_sub_type::play:
                        handle_get_video_play(req, rep);
                        break;
                    case cmd_sub_type::seek:
                        handle_get_video_seek(req, rep);
                        break;
                    case cmd_sub_type::teardown:
                        handle_get_video_teardown(req, rep);
                        break;
                    default:
                        handle_cmd_not_found(req, rep);
                        break;
                }
                break;
            default:
                handle_cmd_not_found(req, rep);
                break;
        }

        if((ret = rep.ByteSize()) > _buf_max_size)
        {
            log("SerializeToArray failed too many bytes");
        } else
        {
            if(rep.SerializeToArray(_buf, _buf_max_size))
            {
                _buf_size = ret;
                if(send() < 0)
                {
                    log("send failed");
                }
            } else
            {
                log("SerializeToArray failed");
            }
        }

    } else
    {
        log("ParseFromArray failed");
    }
}

void nn_rep::step(nn_pollfd *pfd) {
    if(pfd->revents & NN_POLLIN)
        step();
}

void nn_rep::handle_push_video(const media_server_req& req, media_server_rep& rep) {
    size_t pts;
    timeval tv;

    const video& push_video = req._push_video();
    for(int i = 0; i < push_video._items_size(); ++i)
    {
        const video_item& item = push_video._items(i);
        h264_key key(item._key());
        pts = item._pts();
        tv.tv_sec = pts / TIME_MS_BASE;  //TIME_US_BASE
        tv.tv_usec = pts % TIME_MS_BASE; //TIME_US_BASE
        pts = gen_pts(&tv);
        const_cast<video_item&>(item).set__pts(pts);


        h264_dump* dump = _dump_map.add_or_get_cache_dump(key);
        if(dump == 0)
        {
            log("add_or_get_cache_dump failed");
        }
        else
        {
            dump->step((const uint8_t*)&*item._nalu().begin(), item._nalu().size(), item._pts());
        }

        // pub service
        int lab, dev, chn;
        key.parse_cache_key(&lab, &dev, &chn);
        printf("pub: key=%02d%02d%02d, pts=%zu\n", lab, dev, chn, item._pts());
        //print_buf((const uint8_t*)&*item._nalu().begin(), item._nalu().size());
        _pub.publish(key, (const uint8_t*)&*item._nalu().begin(), item._nalu().size(), item._pts());
    }
    rep.set__type(cmd_type::push_video);
}

nn_pollfd nn_rep::pool_fd() {
    return nn_pollfd{ .fd = _sock, .events = NN_POLLIN };
}

void nn_rep::handle_get_avl_video(const media_server_req &req, media_server_rep &rep) {
}

void nn_rep::handle_get_video_setup(const media_server_req &req, media_server_rep &rep) {
    const media_server_req_get_video_setup& setup_req = req._get_video_setup();
    const video_key& setup_req_key = setup_req._key();
    media_server_rep_get_video_setup* setup_rep = rep.mutable__get_video_setup();
    size_t id;
    if(_session_map.setup(&id, setup_req_key._lab(), setup_req_key._dev(), setup_req_key._chn(),
                          setup_req._begin_pts(), setup_req._end_pts()) < 0)
    {
        setup_rep->set__status(-1);
    } else
    {
        setup_rep->set__status(0);
        setup_rep->set__session(id);
    }
    rep.set__type(cmd_type::get_video);
    rep.set__sub_type(cmd_sub_type::setup);
}

void nn_rep::handle_get_video_play(const media_server_req &req, media_server_rep &rep) {
    const media_server_req_get_video_play& play_req = req._get_video_play();
    media_server_rep_get_video_play* play_rep = rep.mutable__get_video_play();
    size_t id;
    void* data;
    size_t data_size;
    size_t pts;

    id = play_req._session();

    if(_session_map.play(id, &data, &data_size, &pts) < 0)
    {
        play_rep->set__status(-1);
    } else
    {
        play_rep->set__status(0);
        play_rep->set__nalu(data, data_size);
        play_rep->set__pts(pts);
    }
    play_rep->set__session(id);
    rep.set__type(cmd_type::get_video);
    rep.set__sub_type(cmd_sub_type::play);
}

void nn_rep::handle_get_video_seek(const media_server_req &req, media_server_rep &rep) {
    const media_server_req_get_video_seek& seek_req = req._get_video_seek();
    media_server_rep_get_video_seek* seek_rep = rep.mutable__get_video_seek();
    size_t id;
    void* data;
    size_t data_size;
    size_t pts;

    id = seek_req._session();
    pts = seek_req._pts();
    if(_session_map.seek(id, pts) < 0)
    {
        seek_rep->set__status(-1);
    } else
    {
        seek_rep->set__status(0);
    }
    seek_rep->set__session(id);
    rep.set__type(cmd_type::get_video);
    rep.set__sub_type(cmd_sub_type::seek);
}

void nn_rep::handle_get_video_teardown(const media_server_req &req, media_server_rep &rep) {
    const media_server_req_get_video_teardown& teardown_req = req._get_video_teardown();
    media_server_rep_get_video_teardown* teardown_rep = rep.mutable__get_video_teardown();
    size_t id;
    void* data;
    size_t data_size;
    size_t pts;

    id = teardown_req._session();
    if(_session_map.teardown(id) < 0)
    {
        teardown_rep->set__status(-1);
    } else
    {
        teardown_rep->set__status(0);
    }
    teardown_rep->set__session(id);
    rep.set__type(cmd_type::get_video);
    rep.set__sub_type(cmd_sub_type::teardown);
}

void nn_rep::handle_cmd_not_found(const media_server_req &req, media_server_rep &rep) {
    throw;
}

nn_rep::nn_rep(const args_info *args) :
        nn_rep(args->_rep_url.c_str(), args->_rep_remote_repos.c_str(),
            args->_rep_local_repos.c_str(), args->_pub_url.c_str(),
            args->_rep_sample_264_format.c_str()) {

}
