//
// Created by rio on 1/31/18.
//

#include <sys/time.h>
#include "util.h"

void console_log::log(const char *where, const char *what, const char *fmt, ...)
{
    va_list va;
    va_start(va, fmt);
    vlog(where, what, fmt, va);
    va_end(va);
}

void console_log::vlog(const char *where, const char *what, const char *fmt, va_list va)
{
    char buf[0x200];
    int pos;
    time_t now;
    now = time(0);
    ctime_r(&now, buf);
    pos = snprintf(buf + 20, sizeof(buf) - 20, "|%-30s | %-20s | ", where, what);
    pos += 20;
    if(fmt)
    {
        pos += vsnprintf(buf + pos, sizeof(buf) - pos, fmt, va);
    }
    snprintf(buf + pos, sizeof(buf) - pos, "\n");
    {
        std::lock_guard<std::mutex> lock{_mtx};
        printf(buf);
    }
}


console_log _g_default_log;

void print_buf(const uint8_t *buf, int buf_size) {

    int size = buf_size > 0x40 ? 0x40 : buf_size;

    printf("%05d:", buf_size);
    for(int i = 0; i < size; ++i)
        printf("%02X ", *(buf + i));
    printf("\n");
}

size_t gen_pts(uint32_t sec, uint32_t ms, const AVRational* time_base)
{
    AVRational default_time_base = {1, TIME_US_BASE};
    size_t pts;

    if(time_base == 0)
    {
        time_base = &default_time_base;
    }

    pts = sec;
    pts *= TIME_MS_BASE;
    pts += ms;
    pts *= TIME_MS_BASE;
    pts = pts / (av_q2d(*time_base) * TIME_US_BASE);

    return pts;
}
size_t gen_pts(const timeval* tv, const AVRational* time_base)
{
    AVRational default_time_base = {1, TIME_US_BASE};
    size_t pts;

    if(time_base == 0)
    {
        time_base = &default_time_base;
    }

    pts = tv->tv_sec;
    pts *= TIME_MS_BASE;
    pts += tv->tv_usec;
    pts = pts / (av_q2d(*time_base) * TIME_US_BASE);

    return pts;
}
void parse_pts(size_t pts, time_t* time, const AVRational* time_base)
{
    AVRational default_time_base = {1, TIME_US_BASE};

    if(time_base == 0)
    {
        time_base = &default_time_base;
    }
    pts = pts * (av_q2d(*time_base) * TIME_US_BASE); //us
    *time = pts / TIME_US_BASE;
}
void parse_pts(size_t pts, timeval* tv, const AVRational* time_base)
{
    AVRational default_time_base = {1, TIME_US_BASE};

    if(time_base == 0)
    {
        time_base = &default_time_base;
    }
    pts = pts * (av_q2d(*time_base) * TIME_US_BASE); //us
    tv->tv_sec = pts / TIME_US_BASE;
    tv->tv_usec = pts % TIME_US_BASE;
}

size_t gen_pts(const time_t *time, const AVRational *time_base) {
    AVRational default_time_base = {1, TIME_US_BASE};
    size_t pts;

    if(time_base == 0)
    {
        time_base = &default_time_base;
    }

    pts = *time;
    pts *= TIME_US_BASE;
    pts = pts / (av_q2d(*time_base) * TIME_US_BASE);

    return pts;
}

time_t make_time(int y, int mon, int d, int h, int min, int s) {
    tm t;
    t.tm_year = y - 1900;
    t.tm_mon = mon - 1;
    t.tm_mday = d;
    t.tm_hour = h;
    t.tm_min = min;
    t.tm_sec = s;

    return mktime(&t);
}

int diff_time(const timeval* tv1, const timeval* tv2) {
    size_t us1 = tv1->tv_sec * TIME_US_BASE + tv1->tv_usec;
    size_t us2 = tv2->tv_sec * TIME_US_BASE + tv2->tv_usec;

    return us1 > us2 ? us1 - us2 : -(us2 - us1);
}

size_t get_now_us() {
    timeval now_tv;
    gettimeofday(&now_tv, 0);
    return now_tv.tv_usec + now_tv.tv_sec * TIME_US_BASE;
}
