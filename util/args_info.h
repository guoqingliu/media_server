//
// Created by rio on 4/16/18.
//

#ifndef MEDIA_SERVER_XML_CONFIGURE_H
#define MEDIA_SERVER_XML_CONFIGURE_H

#include <string>
#include <tinyxml2.h>

/*<config>
    <pub>
        <url>tcp://192.168.1.212:1213</url>
    </pub>
    <rep>
        <url>tcp://192.168.1.212:1213</url>
        <remote_repos>/file/611/mp4</remote_repos>
        <local_repos>/home/rio/video/611/mp4</local_repos>
        <hdfs>hdfs://192.168.1.213:8020</hdfs>
    </rep>
    <sub>
        <recv_timeout>200</recv_timeout>
        <send_timeout>200</send_timeout>
    </sub>
    <req>
        <recv_timeout>200</recv_timeout>
        <send_timeout>200</send_timeout>
    </req>
</config>
 * */

#define PARSE_NODE(args, root, clss, item) \
    if(parse_from_node(root, #clss, #item, args->_##clss##_##item) < 0) \
        return -1;

using tinyxml2::XMLDocument;
using tinyxml2::XMLElement;
using tinyxml2::XMLError;
class args_info
{
private:
    args_info(){
        if(parse_from_xml() < 0)
            throw;
    }

    int parse_from_node(const XMLElement* base, const char* clss, const char* item, std::string& des)
    {
        const XMLElement* pos;
        const char* value;
        pos = base;

        if((pos = pos->FirstChildElement(clss)) == 0)
            return -1;
        if((pos = pos->FirstChildElement(item)) == 0)
            return -1;
        des = (value = pos->GetText()) ? value : "";
        return 0;
    }

    int parse_from_xml(const char* xml_file = "configure.xml")
    {

        XMLDocument doc;
        const XMLElement* root;
        if(doc.LoadFile(xml_file) != XMLError::XML_SUCCESS)
            return -1;
        if((root = doc.RootElement()) == 0)
            return -1;

        PARSE_NODE(this, root, pub, url);
        PARSE_NODE(this, root, rep, url);
        PARSE_NODE(this, root, rep, local_repos);
        PARSE_NODE(this, root, rep, remote_repos);
        PARSE_NODE(this, root, rep, sample_264_format);
        PARSE_NODE(this, root, req, sample_264_format);
        PARSE_NODE(this, root, sub, recv_timeout);
        PARSE_NODE(this, root, sub, send_timeout);
        PARSE_NODE(this, root, req, recv_timeout);
        PARSE_NODE(this, root, req, send_timeout);

        return 0;
    }
public:
    static args_info* get(){
        static args_info _g_args;
        return &_g_args;
    }
public:
    std::string _pub_url;
    std::string _rep_url;
    std::string _rep_local_repos;
    std::string _rep_remote_repos;
    std::string _rep_sample_264_format;
    std::string _req_sample_264_format;
    std::string _sub_recv_timeout;
    std::string _sub_send_timeout;
    std::string _req_recv_timeout;
    std::string _req_send_timeout;
};



#endif //MEDIA_SERVER_XML_CONFIGURE_H
