//
// Created by rio on 3/6/18.
//

#ifndef MEDIA_SERVER_NN_REQ_REP_TEST_H
#define MEDIA_SERVER_NN_REQ_REP_TEST_H

#include <thread>
#include "../service/nn_req.h"
#include "../service/nn_rep.h"

void media_server_test(int argc, char** argv);
void push_video_test(int argc, char** argv);
//void media_player_sub_test(int argc, char** argv);
//void media_player_req_test(int argc, char** argv);
#endif //MEDIA_SERVER_NN_REQ_REP_TEST_H
