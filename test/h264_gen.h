//
// Created by rio on 4/27/18.
//

#ifndef MEDIA_SERVER_H264_GEN_H
#define MEDIA_SERVER_H264_GEN_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
extern "C"
{
#include <libavcodec/avcodec.h>
#include <libavutil/opt.h>
#include <libavutil/imgutils.h>
};

#include <string>
#include <getopt.h>

typedef struct
{
    std::string _output_file;
    int _height;
    int _width;
    int _frame_rate;
    int _seconds;
    int _bit_rate;
}cmd_args_t;


static void encode(AVCodecContext *enc_ctx, AVFrame *frame, AVPacket *pkt,
                   FILE *outfile)
{
    int ret;
    /* send the frame to the encoder */
    ret = avcodec_send_frame(enc_ctx, frame);
    if (ret < 0) {
        fprintf(stderr, "Error sending a frame for encoding\n");
        exit(1);
    }
    while (ret >= 0) {
        ret = avcodec_receive_packet(enc_ctx, pkt);
        if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
            return;
        else if (ret < 0) {
            fprintf(stderr, "Error during encoding\n");
            exit(1);
        }
        fwrite(pkt->data, 1, pkt->size, outfile);
        av_packet_unref(pkt);
    }
}

void cmd_args_parse(cmd_args_t* args, int argc, char** argv)
{
    args->_width = 1920;
    args->_height = 1080;
    args->_frame_rate = 25;
    args->_bit_rate = 400 * 1024;
    args->_seconds = 1;
    args->_output_file = "default.264";

    char c;

    while((c = getopt(argc, argv, "w:h:f:s:o:b:")) != -1)
    {
        switch(c)
        {
            case 'w':
                args->_width = atoi(optarg);
                break;
            case 'h':
                args->_height = atoi(optarg);
                break;
            case 'f':
                args->_frame_rate = atoi(optarg);
                break;
            case 's':
                args->_seconds = atoi(optarg);
                break;
            case 'b':
                args->_bit_rate = atoi(optarg);
                break;
            case 'o':
                args->_output_file = optarg;
                break;
            default:
                printf("invalid arg option(-%c)\n", c);
                break;
        }
    }

}

int main(int argc, char **argv)
{
    char err_buf[AV_ERROR_MAX_STRING_SIZE];
    cmd_args_t args;
    const AVCodec *codec;
    AVCodecContext *codec_ctx= 0;
    int i, ret, x, y;
    FILE *f;
    AVFrame *frame;
    AVPacket *pkt;
    int rand_base;

    avcodec_register_all();

    codec = avcodec_find_encoder(AVCodecID::AV_CODEC_ID_H264);
    codec_ctx = avcodec_alloc_context3(codec);
    if (!codec_ctx) {
        fprintf(stderr, "Could not allocate video codec context\n");
        exit(1);
    }

    pkt = av_packet_alloc();
    if (!pkt)
        exit(1);

    cmd_args_parse(&args, argc, argv);
    /* put sample parameters */
    codec_ctx->bit_rate = args._bit_rate;
    /* resolution must be a multiple of two */
    codec_ctx->width = args._width;
    codec_ctx->height = args._height;
    /* frames per second */
    codec_ctx->time_base = (AVRational){1, args._frame_rate};
    codec_ctx->framerate = (AVRational){args._frame_rate, 1};
    codec_ctx->gop_size = 10;
    codec_ctx->max_b_frames = 1;
    codec_ctx->pix_fmt = AV_PIX_FMT_YUV420P;
    if (codec->id == AV_CODEC_ID_H264)
        av_opt_set(codec_ctx->priv_data, "preset", "slow", 0);
    /* open it */

    ret = avcodec_open2(codec_ctx, codec, NULL);
    if (ret < 0) {
        fprintf(stderr, "Could not open codec: %s\n", av_make_error_string(err_buf, sizeof(err_buf), ret));
        exit(1);
    }

    f = fopen(args._output_file.c_str(), "wb");
    if (!f) {
        fprintf(stderr, "Could not open %s\n", args._output_file.c_str());
        exit(1);
    }

    frame = av_frame_alloc();
    if (!frame) {
        fprintf(stderr, "Could not allocate video frame\n");
        exit(1);
    }

    frame->format = codec_ctx->pix_fmt;
    frame->width  = codec_ctx->width;
    frame->height = codec_ctx->height;

    ret = av_frame_get_buffer(frame, 32);
    if (ret < 0) {
        fprintf(stderr, "Could not allocate the video frame data\n");
        exit(1);
    }

    srand(time(0));

    rand_base = rand() % 100;
    for (i = 0; i < args._frame_rate * args._seconds; i++)
    {
        ret = av_frame_make_writable(frame);
        if (ret < 0)
            exit(1);
        for (y = 0; y < codec_ctx->height; y++) {
            for (x = 0; x < codec_ctx->width; x++) {
                frame->data[0][y * frame->linesize[0] + x] = x + y - i  + rand_base;
            }
        }
        for (y = 0; y < codec_ctx->height/2; y++) {
            for (x = 0; x < codec_ctx->width/2; x++) {
                frame->data[1][y * frame->linesize[1] + x] = 128 + y - i * 2;
                frame->data[2][y * frame->linesize[2] + x] = 64 + x - i * 5;
            }
        }
        frame->pts = i;
        encode(codec_ctx, frame, pkt, f);
    }
    encode(codec_ctx, NULL, pkt, f);
    fclose(f);
    avcodec_free_context(&codec_ctx);
    av_frame_free(&frame);
    av_packet_free(&pkt);
    return 0;
}
#endif //MEDIA_SERVER_H264_GEN_H
