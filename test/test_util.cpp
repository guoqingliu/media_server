//
// Created by rio on 3/6/18.
//

#include "test_util.h"
#include "../service/nn_helper_set.h"
#include "../service/h264_parse.h"

nn_helper_set* _g_services = 0;
nn_req* _g_push_video_req = 0;
void sig_int_handler(int sig)
{
    if(_g_services)
    {
        _g_services->break_loop();
    }

    if(_g_push_video_req)
    {
        _g_push_video_req->break_loop();
    }
}

void media_server_test(int argc, char** argv)
{
    args_info* args = args_info::get();    //read xml configure
    av_register_all();                     //init ffmpeg
    nn_helper_set services;                // all serrvices manager(rep pub dump)
    _g_services = &services;
    nn_rep rep(args);                      //rep service
    signal(SIGINT, sig_int_handler);
    std::thread{[&](){
        services.add_helper(&rep);
        //services.add_helper(&pub);
        services.loop();
    }}.join();
    //pub.dump();
    rep.dump();
}

void push_video_test(int argc, char** argv)
{
    args_info* args = args_info::get();
    nn_req push_video_req(args);
    _g_push_video_req = &push_video_req;
    signal(SIGINT, sig_int_handler);
    std::thread{[&](){
		push_video_req.push_h264_file(args->_req_sample_264_format.c_str());
    }}.join();
}